﻿namespace MMM_IDEditor {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxLevel = new System.Windows.Forms.TextBox();
            this.buttonLevel = new System.Windows.Forms.Button();
            this.labelMMLDir = new System.Windows.Forms.Label();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.openFileLevel = new System.Windows.Forms.OpenFileDialog();
            this.labelMessage = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.idBeforeImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.idBefore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idAfterImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.idAfter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescription.Location = new System.Drawing.Point(33, 9);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(308, 31);
            this.labelDescription.TabIndex = 1;
            this.labelDescription.Text = "Description";
            this.labelDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxLevel
            // 
            this.textBoxLevel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBoxLevel.Enabled = false;
            this.textBoxLevel.Location = new System.Drawing.Point(12, 63);
            this.textBoxLevel.Multiline = true;
            this.textBoxLevel.Name = "textBoxLevel";
            this.textBoxLevel.Size = new System.Drawing.Size(350, 35);
            this.textBoxLevel.TabIndex = 2;
            // 
            // buttonLevel
            // 
            this.buttonLevel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.buttonLevel.Location = new System.Drawing.Point(12, 104);
            this.buttonLevel.Name = "buttonLevel";
            this.buttonLevel.Size = new System.Drawing.Size(49, 20);
            this.buttonLevel.TabIndex = 3;
            this.buttonLevel.Text = "...";
            this.buttonLevel.UseVisualStyleBackColor = true;
            this.buttonLevel.Click += new System.EventHandler(this.buttonLevel_Click);
            // 
            // labelMMLDir
            // 
            this.labelMMLDir.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelMMLDir.Location = new System.Drawing.Point(9, 40);
            this.labelMMLDir.Name = "labelMMLDir";
            this.labelMMLDir.Size = new System.Drawing.Size(91, 20);
            this.labelMMLDir.TabIndex = 4;
            this.labelMMLDir.Text = "Mega Man Level";
            this.labelMMLDir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "metal.png");
            this.imageList.Images.SetKeyName(1, "metalsheets-0.png");
            this.imageList.Images.SetKeyName(2, "metalsheets-1.png");
            this.imageList.Images.SetKeyName(3, "bigrock-0.png");
            this.imageList.Images.SetKeyName(4, "bigrock-1.png");
            this.imageList.Images.SetKeyName(5, "bigrock-2.png");
            this.imageList.Images.SetKeyName(6, "doortech-0.png");
            this.imageList.Images.SetKeyName(7, "doortech-1.png");
            this.imageList.Images.SetKeyName(8, "doortech-2.png");
            this.imageList.Images.SetKeyName(9, "bossdoor.png");
            this.imageList.Images.SetKeyName(10, "ladder.png");
            this.imageList.Images.SetKeyName(11, "spikes.png");
            this.imageList.Images.SetKeyName(12, "bars-0.png");
            this.imageList.Images.SetKeyName(13, "bars-1.png");
            this.imageList.Images.SetKeyName(14, "wall-0.png");
            this.imageList.Images.SetKeyName(15, "wall-1.png");
            this.imageList.Images.SetKeyName(16, "wall-2.png");
            this.imageList.Images.SetKeyName(17, "wall-3.png");
            this.imageList.Images.SetKeyName(18, "wire-0.png");
            this.imageList.Images.SetKeyName(19, "wire-1.png");
            this.imageList.Images.SetKeyName(20, "wire-2.png");
            this.imageList.Images.SetKeyName(21, "cuthutroof-0.png");
            this.imageList.Images.SetKeyName(22, "cuthutroof-1.png");
            this.imageList.Images.SetKeyName(23, "cuthutroof-2.png");
            this.imageList.Images.SetKeyName(24, "cuthutdeco-0.png");
            this.imageList.Images.SetKeyName(25, "cuthutdeco-1.png");
            this.imageList.Images.SetKeyName(26, "cuthutside-0.png");
            this.imageList.Images.SetKeyName(27, "cuthutside-1.png");
            this.imageList.Images.SetKeyName(28, "cuthutdoor-0.png");
            this.imageList.Images.SetKeyName(29, "cuthutdoor-1.png");
            this.imageList.Images.SetKeyName(30, "cuthutdoor-2.png");
            this.imageList.Images.SetKeyName(31, "cuthutdoor-3.png");
            this.imageList.Images.SetKeyName(32, "cuthutladder.png");
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdd.Location = new System.Drawing.Point(35, 145);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(30, 20);
            this.buttonAdd.TabIndex = 8;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRemove.Location = new System.Drawing.Point(71, 145);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(30, 20);
            this.buttonRemove.TabIndex = 9;
            this.buttonRemove.Text = "-";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonSave.Enabled = false;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(110, 301);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(155, 41);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "SAVE";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // openFileLevel
            // 
            this.openFileLevel.AddExtension = false;
            this.openFileLevel.Filter = "Text files|*.txt|Maker files|*.mml|All files|*.*";
            this.openFileLevel.InitialDirectory = "C:\\";
            this.openFileLevel.RestoreDirectory = true;
            this.openFileLevel.ShowHelp = true;
            this.openFileLevel.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileLevel_FileOk);
            // 
            // labelMessage
            // 
            this.labelMessage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelMessage.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelMessage.Location = new System.Drawing.Point(23, 345);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(329, 36);
            this.labelMessage.TabIndex = 11;
            this.labelMessage.Text = "Message";
            this.labelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idBeforeImage,
            this.idBefore,
            this.idAfterImage,
            this.idAfter});
            this.dataGridView.Location = new System.Drawing.Point(35, 171);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.ShowEditingIcon = false;
            this.dataGridView.Size = new System.Drawing.Size(305, 113);
            this.dataGridView.TabIndex = 12;
            // 
            // idBeforeImage
            // 
            this.idBeforeImage.HeaderText = "";
            this.idBeforeImage.Name = "idBeforeImage";
            this.idBeforeImage.ReadOnly = true;
            this.idBeforeImage.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.idBeforeImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idBeforeImage.Width = 20;
            // 
            // idBefore
            // 
            this.idBefore.HeaderText = "ID Before";
            this.idBefore.Name = "idBefore";
            this.idBefore.ReadOnly = true;
            this.idBefore.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.idBefore.Width = 122;
            // 
            // idAfterImage
            // 
            this.idAfterImage.HeaderText = "";
            this.idAfterImage.Name = "idAfterImage";
            this.idAfterImage.ReadOnly = true;
            this.idAfterImage.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.idAfterImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.idAfterImage.Width = 20;
            // 
            // idAfter
            // 
            this.idAfter.HeaderText = "ID After";
            this.idAfter.Name = "idAfter";
            this.idAfter.ReadOnly = true;
            this.idAfter.Width = 122;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 390);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.labelMMLDir);
            this.Controls.Add(this.buttonLevel);
            this.Controls.Add(this.textBoxLevel);
            this.Controls.Add(this.labelDescription);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Mega Man Maker ID Tool";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxLevel;
        private System.Windows.Forms.Button buttonLevel;
        private System.Windows.Forms.Label labelMMLDir;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.OpenFileDialog openFileLevel;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewImageColumn idBeforeImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn idBefore;
        private System.Windows.Forms.DataGridViewImageColumn idAfterImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAfter;
    }
}

