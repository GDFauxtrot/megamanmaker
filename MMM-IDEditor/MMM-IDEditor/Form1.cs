﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Timers;
using System.Windows.Forms;

namespace MMM_IDEditor {
    public partial class Form1 : Form {

        List<string> greetings;
        Form2 form2;

        bool saveEnabled;
        Timer messageTimer;

        public Form1() {
            InitializeComponent();
            greetings = new List<string>() {
                "Oh fuck, not you again.",
                "Messed with the ID database again, did we?",
                "You probably fucked something up.",
                "The fact that I'm open means you messed something up. Good going.",
                "*notices your burgeoning ID database* OwO what's this?",
                "Another one bites the dust!",
                "Aww, fuck. I can't believe you done this.",
                "I exist solely to fix your shitty mistakes. It is an existence of suffering."
            };

            Random rand = new Random();
            labelDescription.Text = greetings[rand.Next(0, greetings.Count)];
            labelMessage.Text = "";
        }

        private void buttonLevel_Click(object sender, EventArgs e) {
            openFileLevel.ShowDialog();
        }

        private void openFileLevel_FileOk(object sender, CancelEventArgs e) {
            // TODO MML header checking!

            Stream openStream = openFileLevel.OpenFile();
            using (BinaryReader reader = new BinaryReader(openStream)) {
                if (Encoding.ASCII.GetString(reader.ReadBytes(7)) != "MMMAKER") {
                    MessageBox.Show("The selected file is either invalid\n" +
                        "(not in Mega Man Maker format) or corrupted!\n\nPlease select another file.",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }

            if (!e.Cancel) {
                openStream.Close();
                textBoxLevel.Text = openFileLevel.FileName;
                CheckSaveButtonEnabled();
            }
        }

        public void AddToIDList(int from, int to) {
            //ListViewItem existingItem = listView.FindItemWithText(from.ToString());
            //if (existingItem == null || existingItem.Text != from.ToString()) {
            //    ListViewItem item = new ListViewItem(from.ToString());
            //    item.ImageIndex = from - 1;
            //    item.SubItems.Add(to.ToString());
            //    listView.Items.Add(item);
            //} else {
            //    existingItem.SubItems[1].Text = to.ToString();
            //}
            bool found = false;
            foreach (DataGridViewRow rowSearch in dataGridView.Rows) {
                if (rowSearch.Cells[1].Value.ToString() == from.ToString()) {
                    found = true;
                    rowSearch.Cells[2].Value = imageList.Images[to - 1];
                    rowSearch.Cells[3].Value = to;
                }
            }

            if (!found) {
                Image fromImg = from <= imageList.Images.Count ? imageList.Images[from - 1] : null;
                Image toImg = to <= imageList.Images.Count ? imageList.Images[to - 1] : null;
                object[] row = new object[4] { fromImg, from, toImg, to };
                dataGridView.Rows.Add(row);
            }

            CheckSaveButtonEnabled();
        }

        private void buttonAdd_Click(object sender, EventArgs e) {
            if (form2 == null) {
                form2 = new Form2(this);
            }
            form2.ShowDialog();
        }

        private void buttonRemove_Click(object sender, EventArgs e) {
            foreach (DataGridViewRow item in dataGridView.SelectedRows) {
                dataGridView.Rows.Remove(item);
            }

            CheckSaveButtonEnabled();
        }

        private void CheckSaveButtonEnabled() {
            saveEnabled = textBoxLevel.Text != "" && dataGridView.Rows.Count > 0;

            buttonSave.Enabled = saveEnabled;
        }

        private void buttonSave_Click(object sender, EventArgs e) {

            List<ushort> oldData = new List<ushort>();

            int backupNum = 1;
            while (File.Exists(openFileLevel.FileName + ".backup" + backupNum.ToString("D3"))) {
                backupNum++;
            }

            string backupStr = ".backup" + backupNum.ToString("D3");

            Console.WriteLine(backupStr);

            Stream openStream = openFileLevel.OpenFile();
            using (BinaryReader reader = new BinaryReader(openStream)) {
                using (BinaryWriter writer = new BinaryWriter(File.Create(textBoxLevel.Text + backupStr))) {
                    while (reader.PeekChar() != -1) {
                        ushort data = reader.ReadUInt16();
                        oldData.Add(data);
                        writer.Write(data);
                    }
                }
            }

            openStream.Close();

            using (BinaryWriter writer = new BinaryWriter(File.Create(textBoxLevel.Text))) {
                int headerCounter = 0;
                bool FFFFlast = false, FFFFafterID = false;
                foreach (ushort oldByte in oldData) {
                    if (headerCounter < 8) {
                        // Write previous header without checking for ID replacements
                        writer.Write(oldByte);
                        headerCounter++;
                    } else {
                        if (FFFFlast && FFFFafterID) {
                            FFFFlast = false;
                            FFFFafterID = false;
                            writer.Write(oldByte);
                            continue;
                        }
                        if (FFFFlast && !FFFFafterID) {
                            FFFFafterID = true;
                        }
                        if (oldByte == ushort.MaxValue) {
                            FFFFlast = true;
                            writer.Write(oldByte);
                            continue;
                        }
                        bool found = false;
                        foreach (DataGridViewRow idRow in dataGridView.Rows) {
                            if (oldByte == System.Convert.ToUInt16(idRow.Cells[1].Value)) {
                                writer.Write(System.Convert.ToUInt16(idRow.Cells[3].Value));
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            writer.Write(oldByte);
                        }
                    }
                }
            }

            string backupFile = openFileLevel.SafeFileName + backupStr;
            Console.WriteLine(backupFile);
            labelMessage.Text = "File '" + openFileLevel.SafeFileName + "' saved. Backup file is '" + backupFile + "'.";

            messageTimer = new Timer();
            messageTimer.Tick += new EventHandler(OnTimedEvent);
            messageTimer.Interval = 10000;
            messageTimer.Enabled = true;

        }

        private void OnTimedEvent(object source, EventArgs e) {
            labelMessage.Text = "";
            messageTimer.Enabled = false;
        }
    }
}
