﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMM_IDEditor {
    public partial class Form2 : Form {

        public ushort fromID, toID;

        bool ignoreChange, saveIDsConfirmed;
        bool okEnabled;

        Form1 form1;

        public Form2(Form1 form1) {
            InitializeComponent();
            this.form1 = form1;
            AcceptButton = buttonOK;
        }

        private void Form2_OnClosing(object sender, EventArgs e) {
            if (saveIDsConfirmed) {
                form1.AddToIDList(fromID, toID);
            }
            saveIDsConfirmed = false;

            okEnabled = false;
            buttonOK.Enabled = false;

            fromID = 0;
            toID = 0;

            textBoxFrom.Select();

            ignoreChange = true;
            textBoxFrom.Text = "0";
            textBoxTo.Text = "0";
            ignoreChange = false;

        }

        private void textBoxFrom_TextChanged(object sender, EventArgs e) {
            TextBoxChanged(textBoxFrom, ref fromID);
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            saveIDsConfirmed = true;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e) {
            saveIDsConfirmed = false;
            Close();
        }

        private void textBoxTo_TextChanged(object sender, EventArgs e) {
            TextBoxChanged(textBoxTo, ref toID);
        }

        private void TextBoxChanged(TextBox textBox, ref ushort outID) {

            // Editing the TextBox text (done below) re-fires this event...
            // this flag will prevent that from occuring.
            if (ignoreChange)
                return;

            ignoreChange = true;
            int cursorPos = textBox.SelectionStart;
            while (!ushort.TryParse(textBox.Text, out outID) && textBox.Text.Length > 0) {
                if (textBox.SelectionStart == textBox.Text.Length) {
                    textBox.Text = textBox.Text.Remove(textBox.SelectionStart - 1, 1);
                } else {
                    textBox.Text = textBox.Text.Remove(--cursorPos, 1);
                }
            }
            if (cursorPos >= textBox.Text.Length) {
                textBox.SelectionStart = textBox.Text.Length;
            } else {
                textBox.SelectionStart = cursorPos;
            }

            string fromHex = fromID.ToString("X4");
            string toHex = toID.ToString("X4");

            labelHexFrom.Text = "HxD Hex: " + fromHex.Substring(2,2) + " " + fromHex.Substring(0, 2);
            labelHexTo.Text = "HxD Hex: " + toHex.Substring(2, 2) + " " + toHex.Substring(0, 2);

            okEnabled = fromID > 0 && toID > 0 && fromID != toID;
            buttonOK.Enabled = okEnabled;

            ignoreChange = false;
        }
    }
}
