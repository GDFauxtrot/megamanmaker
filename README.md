# README #

This is more of a collection of notes, TODO's and blurbs used to organize my thoughts, than a proper README.
If you are an employer or whatnot looking at my current work, then I encourage you to take a look at the source
and ask me for a stable build.

(Note that I will soon just provide a link here to a build, if you're reading this part then I haven't done it yet.)

With all of that said, this is Mega Man Maker. Development started during the Summer of 2015 and has continued off and on since.
The end goal is to provide a set of tools for fans of the classic Mega Man games to create their own levels, with
support for custom sprites, sounds, scripts and bosses. In addition, because reverse engineering ROM data sounded unappealing
and I wanted to introduce more than the NES could handle, work has been done to recreate the original NES Mega Man titles
in Unity, which so far has been fun and yields good results.

Currently, a solid Player controller and extensive system for tiles of various types has been created for the game, as well
as a rudimentary level editing system and file saving/loading. Now, my main goal is to create an extensive toolset for players
to work on their levels. Once that is done, I will then introduce enemies and items into the game with pre-programmed simple AI.
After that, I will introduce a system for adding custom assets to levels, then perhaps a scripting system for players to make
their levels more dynamic, like buttons or other simple logic.

FUTURE planned development includes a lighting and weather engine, a service for sharing levels, and multiplayer
(cooperative and competitive). These are not the main focus of the project, however, and will come much later once
I feel the current tools are fleshed out and almost all features of the original Mega Man titles are implemented.

### Planned (old) ###

* Change Block bar picker to include stage select like NES (3x3 grid of heads)

* Create Block Group system to take care of creating multi-block structures

* Add more planned features.

### Notes ###

Disregard the previous Planned section (it should be removed soon). Here is a list of features that should be done by
the end of March 2017:

* Current codebase cleaned the hell up from the previous rampage to get file IO working with a menu.

* A fixed and organized Block ID menu

* Graphics for panels, tools and buttons

* Options menu with legitimate options now

* Level properties menu in the Build mode (BG gradient, name, description, author)

* A level testing mode in the Build mode so you don't have to exit and go into Play mode and come back every time

---

MEGA MAN LEVEL (.mml) SPECIFICATIONS:

Levels for MMM should be as light as humanly possible, in my opinion. Unless people are adding large images and sounds
to their levels, the base tiles should hardly take up any space, yet be flexible enough to support just about anyone's
wildest imaginations. With that said, here is how levels are to be structured.

Current save file structure is simple: HEADER on top, followed by the LEVEL data.

The HEADER is structured as follows:

    HEADER
    {
        Identifier: "MMMAKER"
	    Version: U8
        Spawn (X): U16
        Spawn (Y): U16
    }

Spawn coordinates are contained in the HEADER for now, there will be a better place for it later.

    LEVEL
    {
        Identifier: "LEVEL"
        Name: STRING
        Author: STRING
        Description: STRING
        Width (X):  U16
        Height (Y): U16
        BGTopColor: U24
        BGBotColor: U24
        Data: U16[Width * Height - Compression]
    }

Level data is stored as a losslessly-compressed collection of 2-byte "blocks". The level data ends when the file
does, though it is possible to determine its end without a terminator and continue adding more info to the file.

STRINGs are implemented, instead of a null terminator like in C, by a length followed by the string.

    STRING
    {
        Length: U32
        Data: U8[Length]
    }

With this current structure, highly complex levels will only take up 2 x WIDTH x HEIGHT bytes, with WIDTH and HEIGHT representing
the total max size of the level from the origin, and levels with horizontally-repeating blocks can compress that amount greatly
(perhaps by half, even more if the level designer designs with the compression in mind).

-- NOTE ABOUT COMPRESSION --

ID COMMANDS: In-hex command identifiers that specify some action to take when parsing the MML data
	FF FF ## ##: Un-compression
		Levels have a lot of repetitive sequences, so a way to reduce level size is to simply compress all that space into a consecutive count. The count represents the amount of space since the last block.