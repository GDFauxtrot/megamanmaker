﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UndoType { ADD, REMOVE, BOTH, MOVE };

public class UndoItem {
    private Dictionary<Vector2, int> addedBlocks;
    private Dictionary<Vector2, int> removedBlocks;
    private List<Vector2> movedBlocks;
    private Vector2 movedOffset;
    private UndoType undoType;

    public UndoItem(UndoType type, Dictionary<Vector2, int> blocks) {
        addedBlocks = new Dictionary<Vector2, int>();
        removedBlocks = new Dictionary<Vector2, int>();
        movedBlocks = new List<Vector2>();
        undoType = type;

        switch(type) {
            case UndoType.ADD:
                foreach (Vector2 v2 in blocks.Keys)
                    addedBlocks.Add(v2, blocks[v2]);
                break;
            case UndoType.REMOVE:
                foreach (Vector2 v2 in blocks.Keys)
                    removedBlocks.Add(v2, blocks[v2]);
                break;
            case UndoType.BOTH:
                Debug.LogError("UndoItem: Type specified " + type + ", but only one list of blocks was given instead of two.");
                break;
            case UndoType.MOVE:
                Debug.LogError("UndoItem: Type specified " + type + ", but an offset Vector2 was not given.");
                break;
        }
    }

    public UndoItem(UndoType type, Dictionary<Vector2, int> addBlocks, Dictionary<Vector2, int> removeBlocks) {
        addedBlocks = new Dictionary<Vector2, int>();
        removedBlocks = new Dictionary<Vector2, int>();
        movedBlocks = new List<Vector2>();
        undoType = type;

        if (type == UndoType.BOTH) {
            foreach (Vector2 v2 in addBlocks.Keys)
                addedBlocks.Add(v2, addBlocks[v2]);
            foreach (Vector2 v2 in removeBlocks.Keys)
                removedBlocks.Add(v2, removeBlocks[v2]);
        } else {
            if (type == UndoType.MOVE) {
                Debug.LogError("UndoItem: Type specified " + type + ", but an offset Vector2 was not given.");
            } else {
                Debug.LogError("UndoItem: Type specified " + type + ", but two lists of blocks were given instead of one.");
            }
        }
    }

    public UndoItem(UndoType type, List<Vector2> moveBlocks, Vector2 offset, Dictionary<Vector2, int> removeBlocks) {
        addedBlocks = new Dictionary<Vector2, int>();
        removedBlocks = new Dictionary<Vector2, int>();
        movedBlocks = new List<Vector2>();
        undoType = type;

        if (type == UndoType.MOVE) {
            movedBlocks = moveBlocks;
            movedOffset = offset;
            foreach (Vector2 v2 in removeBlocks.Keys) {
                removedBlocks.Add(v2, removeBlocks[v2]);
            }
        } else {
            Debug.LogError("UndoItem: Type specified " + type + ", but the MOVE constructor was used instead.");
        }
    }

    public UndoType GetUndoType() {
        return undoType;
    }

    public Dictionary<Vector2, int> GetAddedBlocks() {
        if (undoType == UndoType.REMOVE || undoType == UndoType.MOVE)
            Debug.LogWarning("This UndoItem (" + undoType + ") does not utilize added blocks!");
        return addedBlocks;
    }

    public Dictionary<Vector2, int> GetRemovedBlocks() {
        if (undoType == UndoType.ADD)
            Debug.LogWarning("This UndoItem (" + undoType + ") does not utilize removed blocks!");
        return removedBlocks;
    }

    public List<Vector2> GetMovedBlocks() {
        if (undoType != UndoType.MOVE)
            Debug.LogWarning("This UndoItem (" + undoType + ") does not utilize moved blocks!");
        return movedBlocks;
    }

    public Vector2 GetOffset() {
        if (undoType != UndoType.MOVE)
            Debug.LogWarning("This UndoItem (" + undoType + ") does not utilize a moved blocks offset vector!");
        return movedOffset;
    }
}
