﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColor {
    private Color colorTop;
    private Color colorBottom;

    public BackgroundColor() {
        colorTop = Color.white;
        colorBottom = Color.white;
    }

    public BackgroundColor(Color top) {
        colorTop = top;
        colorBottom = top;
    }

    public BackgroundColor(Color top, Color bottom) {
        colorTop = top;
        colorBottom = bottom;
    }

    public Color top() {
        return colorTop;
    }

    public Color bottom() {
        return colorBottom;
    }

    public void setTop(Color c) {
        colorTop = c;
    }

    public void setBottom(Color c) {
        colorBottom = c;
    }
}
