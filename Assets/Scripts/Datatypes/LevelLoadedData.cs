﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoadedData{

    public string levelName, levelAuthor, levelDescription;
    public Vector2 spawn;
    public Vector2 levelSize;
    public Color topColor, bottomColor;

    public List<GameObject> loadedBlocks;

    public LevelLoadedData() {
        //loadedBlocks = new List<GameObject>();
    }
}
