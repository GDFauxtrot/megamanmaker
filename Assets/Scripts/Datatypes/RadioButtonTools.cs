﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RadioTool { SELECT, PENCIL, SHAPE, ENTITY, ITEM, NONE };

public class RadioButtonTools {
    private RadioTool activeButton;

    public RadioButtonTools() {
        activeButton = RadioTool.PENCIL;
    }

    public void SetActive(RadioTool button) {
        activeButton = button;
        //switch (button) {
        //    case RadioTool.SELECT:
        //        activeButton = RadioTool.SELECT;
        //        break;
        //    case RadioTool.PENCIL:
        //        activeButton = RadioTool.PENCIL;
        //        break;
        //    case RadioTool.SHAPE:
        //        activeButton = RadioTool.SHAPE;
        //        break;
        //    case RadioTool.ENTITY:
        //        activeButton = RadioTool.ENTITY;
        //        break;
        //    case RadioTool.ITEM:
        //        activeButton = RadioTool.ITEM;
        //        break;
        //}
    }

    public RadioTool GetActive() {
        return activeButton;
    }
}
