﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ShapeRadioButton { OUTLINE, FILLED };

public class ShapeOptionsMenu : MonoBehaviour {
    private float yVelocity;
    private float dTime;
    private RectTransform shapeButton;
    private float initY, showY, targetY;
    private bool shown;

    public ShapeRadioButton selected;

    public int snappiness = 5;

    EditorController editorController;

	void Start () {
        shapeButton = GameObject.Find("ShapeButton").GetComponent<RectTransform>();
        initY = transform.localPosition.y;
        showY = transform.localPosition.y - (GetComponent<RectTransform>().rect.height + shapeButton.rect.height);
        targetY = initY;
        editorController = Camera.main.GetComponent<EditorController>();
        shown = editorController.shapeToolSelected;
	}
	
	void Update () {
        if (shown && !editorController.shapeToolSelected) {
            targetY = initY;
            dTime = Time.deltaTime * snappiness;
        } else if (!shown && editorController.shapeToolSelected) {
            targetY = showY;
            dTime = Time.deltaTime * snappiness;
        }
        shown = editorController.shapeToolSelected;

        if (dTime < 2f) {
            dTime += Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x,
                Mathf.Lerp(GetComponent<RectTransform>().localPosition.y, targetY, dTime),
                transform.localPosition.z);
        }
	}

    public void OutlineSelected() {
        selected = ShapeRadioButton.OUTLINE;
    }

    public void FilledSelected() {
        selected = ShapeRadioButton.FILLED;
    }
}
