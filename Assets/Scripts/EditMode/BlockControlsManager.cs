﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockControlsManager : MonoBehaviour {

    public GameObject gameDropdown, stageDropdown;

    int mmSelectedGame, mmSelectedStage;
    bool showGroups;

	void Start () {
        mmSelectedGame = gameDropdown.GetComponent<Dropdown>().value;
        mmSelectedStage = stageDropdown.GetComponent<Dropdown>().value;
    }
	
	void Update () {
		
	}

    public void UpdateGame() {
        mmSelectedGame = gameDropdown.GetComponent<Dropdown>().value;
    }

    public void UpdateStage() {
        mmSelectedStage = stageDropdown.GetComponent<Dropdown>().value;
    }
}
