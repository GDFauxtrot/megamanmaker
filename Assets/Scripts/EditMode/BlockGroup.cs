﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockGroupType { LOOPHORIZ, LOOPVERT, PATTERN };

public class BlockGroup {

    BlockGroupType groupType;
    bool loopHasStart, loopHasEnd;
    int loopStart, loopEnd;
    int[] loopMid;
    int[,] pattern;

    public BlockGroup(BlockGroupType type, int startBlock, int endBlock, params int[] midBlocks) {
        groupType = type;

        loopStart = startBlock;
        loopEnd = endBlock;
        loopMid = midBlocks;

        loopHasStart = startBlock != 0;
        loopHasEnd = endBlock != 0;
    }

    public BlockGroup(BlockGroupType type, int[,] blocks) {
        groupType = type;
        pattern = blocks;
    }

    public Dictionary<Vector2, int> GetBlocks(Vector2 start, Vector2 end) {

        bool flipX = end.x < start.x;
        bool flipY = end.y < start.y;

        Vector2 trueStart = new Vector2(flipX ? end.x : start.x, flipY ? end.y : start.y);
        Vector2 trueEnd = new Vector2(flipX ? start.x : end.x, flipY ? start.y : end.y);

        Dictionary<Vector2, int> blocks = new Dictionary<Vector2, int>();

        int loopIndexX = flipX ? pattern.GetLength(0)-1 : 0;
        int loopIndexY = flipY ? pattern.GetLength(1)-1 : 0;
        
        for (int y = (int)trueStart.y; y <= trueEnd.y; y += 16) {
            int indexY = (y - (int)trueStart.y) / 16;
            for (int x = (int)trueStart.x; x <= trueEnd.x; x += 16) {
                int indexX = (x - (int)trueStart.x) / 16;
                if (groupType == BlockGroupType.LOOPHORIZ) {
                    if (x == trueStart.x) {
                        blocks.Add(new Vector2(x, y), loopStart);
                    } else if (x == trueEnd.x) {
                        blocks.Add(new Vector2(x, y), loopEnd);
                    } else {
                        blocks.Add(new Vector2(x, y), loopMid[loopIndexX]);
                        //if (++loopIndexX == loopMid.Length)
                        //    loopIndexX = 0;
                    }
                } else if (groupType == BlockGroupType.LOOPVERT) {
                    if (y == trueStart.y) {
                        blocks.Add(new Vector2(x, y), loopStart);
                    } else if (y == trueEnd.y) {
                        blocks.Add(new Vector2(x, y), loopEnd);
                    } else {
                        blocks.Add(new Vector2(x, y), loopMid[loopIndexY]);
                        //if (++loopIndexY == loopMid.Length)
                        //    loopIndexY = 0;
                    }
                } else if (groupType == BlockGroupType.PATTERN) {
                    Debug.Log(loopIndexX + " " + loopIndexY);
                    blocks.Add(new Vector2(x, y), pattern[loopIndexY, loopIndexX]);
                    if (flipX) {
                        if (--loopIndexX < 0)
                            loopIndexX = pattern.GetLength(0) - 1;
                    } else {
                        if (++loopIndexX == pattern.GetLength(0))
                            loopIndexX = 0;
                    }
                    
                }
            }
            loopIndexX = flipX ? pattern.GetLength(0) - 1 : 0;
            if (flipY) {
                if (--loopIndexY < 0)
                    loopIndexY = pattern.GetLength(1) - 1;
            } else {
                if (++loopIndexY == pattern.GetLength(1))
                    loopIndexY = 0;
            }
        }
        
        return blocks;
    }

    private int floorToX(float n, int x) {
        return Mathf.FloorToInt(n / x) * x;
    }
}
