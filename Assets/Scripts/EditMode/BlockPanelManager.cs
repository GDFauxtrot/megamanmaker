﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BlockPanelManager : MonoBehaviour {

    public GameObject controls, sizeSlider;
    public GameObject scrollBar; // Just to check for clicks
    public GameObject scrollRect; // To set really epic values

    float itemGap = 20f, itemSize = 90f; // Sizes at row count 3 (default)

    int selectedID;
    float yInitial, yMax;
    RectTransform parentRect;
    Dictionary<int, GameObject> items;

    bool flagWaitUntilLateUpdateForSizeDelta;

    void Start() {
        selectedID = 1;

        items = new Dictionary<int, GameObject>();
        parentRect = transform.parent.GetComponent<RectTransform>();
        yInitial = GetComponent<RectTransform>().sizeDelta.y;

        for (int ID = 1; BlockID.isValidID(ID); ++ID) {
            GameObject block = Instantiate((GameObject)Resources.Load("UIBlockItem"));
            block.GetComponent<BlockPanelItem>().setupPanel(ID);

            block.transform.SetParent(gameObject.transform);
            block.GetComponent<RectTransform>().position = Vector3.zero;
            block.GetComponent<RectTransform>().localScale = Vector3.one;

            items.Add(ID, block);
        }

        setupBlocksPerRow((int)sizeSlider.GetComponent<Slider>().value);

        items[selectedID].GetComponent<BlockPanelItem>().setSelected(true);
    }

    void LateUpdate() {
        if (flagWaitUntilLateUpdateForSizeDelta) {
            flagWaitUntilLateUpdateForSizeDelta = false;
            GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().sizeDelta.x, yInitial + yMax);
        }
    }

    public void UpdateBlocksPerRowFromSlider() {
        setupBlocksPerRow((int)sizeSlider.GetComponent<Slider>().value);
        scrollRect.GetComponent<ScrollRect>().scrollSensitivity = itemGap + itemSize;
    }

    private void setupBlocksPerRow(int count) {

        int xx = 0;
        int yy = -1;

        itemSize = (310 - (count - 1) * itemGap) / (float)count;

        foreach (int ID in items.Keys) {
            xx = (ID - 1) % count;
            if ((ID - 1) % count == 0)
                ++yy;

            items[ID].GetComponent<RectTransform>().localScale = new Vector2(itemSize / 90f, itemSize / 90f);
            items[ID].GetComponent<RectTransform>().anchoredPosition = new Vector3(itemGap + xx * (itemGap + itemSize), -itemGap - yy * (itemGap + itemSize), 0);
        }

        // Block panel should not scroll past this Y value (starts at Y=0, scrolls towards this max)
        yMax = ((yy + 1) * itemSize) + ((yy + 2) * itemGap) - parentRect.rect.height;
        flagWaitUntilLateUpdateForSizeDelta = true;
    }

    public void setSelectedBlockID(int id) {
        items[selectedID].GetComponent<BlockPanelItem>().setSelected(false);
        selectedID = id;
        items[selectedID].GetComponent<BlockPanelItem>().setSelected(true);
    }

    void Update () {
        if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == scrollBar || EventSystem.current.currentSelectedGameObject == sizeSlider) {
            EventSystem.current.SetSelectedGameObject(null);
        }
	}
}
