﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class BlockPanelItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler {

    public int id;

    static Color selected = new Color(0.7f, 0.7f, 0.7f),
        unselected = new Color(0.4f, 0.4f, 0.4f),
        highlighted = new Color(0.3f, 0.3f, 0.3f),
        pressed = new Color(0.2f, 0.2f, 0.2f);

    bool isPressed, isSelected, isHovered;

    float lerpTime;

	void Start () {
        GetComponent<Image>().color = unselected;
	}
	
	void Update () {
        if (lerpTime < 2f) {
            lerpTime += Time.deltaTime;

            float lerp = lerpTime / 0.5f;
            if (isPressed) {
                GetComponent<Image>().color = new Color(
                    Mathf.Lerp(GetComponent<Image>().color.r, pressed.r, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.g, pressed.g, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.b, pressed.b, lerp));
            } else if (isSelected) {
                GetComponent<Image>().color = new Color(
                    Mathf.Lerp(GetComponent<Image>().color.r, selected.r, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.g, selected.g, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.b, selected.b, lerp));
            } else if (isHovered) {
                GetComponent<Image>().color = new Color(
                    Mathf.Lerp(GetComponent<Image>().color.r, highlighted.r, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.g, highlighted.g, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.b, highlighted.b, lerp));
            } else {
                GetComponent<Image>().color = new Color(
                    Mathf.Lerp(GetComponent<Image>().color.r, unselected.r, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.g, unselected.g, lerp),
                    Mathf.Lerp(GetComponent<Image>().color.b, unselected.b, lerp));
            }
        }
	}

    public void setupPanel(int ID) {
        id = ID;
        gameObject.transform.Find("Image").GetComponent<Image>().sprite = Texture2DImporter.LoadBlockSprite(BlockID.GetTextureLocation(ID));
    }


    public void setSelected(bool selected) {
        isSelected = selected;
        lerpTime = 0;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        isHovered = true;
        lerpTime = 0f;
    }

    public void OnPointerExit(PointerEventData eventData) {
        isHovered = false;
        lerpTime = 0f;
    }

    public void OnPointerDown(PointerEventData eventData) {
        isPressed = true;
        lerpTime = 0f;
    }

    public void OnPointerUp(PointerEventData eventData) {
        isPressed = false;
        lerpTime = 0f;

        Camera.main.GetComponent<EditorController>().SetSelectedBlockID((ushort)id);
    }
}
