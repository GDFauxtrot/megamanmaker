﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Tool {

    void Draw(ushort id, Vector2 start, Vector2 end, List<GameObject> added, List<GameObject> removed, int option = 0);
}
