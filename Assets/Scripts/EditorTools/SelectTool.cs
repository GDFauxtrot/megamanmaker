﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectTool : Tool {

    public Vector2 selectionStart, selectionEnd;

    public void Draw(ushort id, Vector2 start, Vector2 end, List<GameObject> added, List<GameObject> removed, int option = 0) {
        int xStart = floorToX(start.x, 16);
        int xEnd = floorToX(end.x, 16);
        int yStart = floorToX(start.y, 16);
        int yEnd = floorToX(end.y, 16);

        if (Input.GetMouseButtonUp(0)) {
            GameObject selection = GameObject.Find("SelectionHighlight");
            if (selection != null)
                GameObject.Destroy(selection);
        } else {
            GameObject selection = GameObject.Find("SelectionHighlight");
            if (selection == null) {
                selection = GameObject.CreatePrimitive(PrimitiveType.Plane);
                selection.name = "SelectionHighlight";
                selection.GetComponent<MeshCollider>().enabled = false;
                selection.GetComponent<MeshRenderer>().receiveShadows = false;
                selection.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                selection.GetComponent<MeshRenderer>().sortingLayerName = "Foreground";
                selection.GetComponent<MeshRenderer>().sortingOrder = 1;
                selection.transform.localRotation = Quaternion.Euler(90, 0, 180);
                selection.GetComponent<MeshRenderer>().material = (Material) Resources.Load("UnlitSolidColor");
                selection.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, Camera.main.GetComponent<EditorController>().highlightFloat);
            }

            int countX = (xEnd > xStart ? (xEnd - xStart) / 16 + 1 : (xStart - xEnd) / 16 + 1);
            int countY = (yEnd > yStart ? (yEnd - yStart) / 16 + 1 : (yStart - yEnd) / 16 + 1);

            selectionStart = new Vector2((xEnd > xStart ? xStart : xEnd), (yEnd > yStart ? yStart : yEnd));
            selectionEnd = new Vector2((xEnd > xStart ? xEnd : xStart), (yEnd > yStart ? yEnd : yStart));
            
            selection.transform.localPosition = new Vector3((xStart + xEnd) / 2f + 8, (yStart + yEnd) / 2f + 8, selection.transform.localPosition.z);
            selection.transform.localScale = new Vector3(1.6f * countX, 1, 1.6f * countY);
            selection.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(countX, countY);
        }
    }

    public static int floorToX(float n, int x) {
        return Mathf.FloorToInt(n / x) * x;
    }
}
