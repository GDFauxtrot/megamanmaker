﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PencilTool : Tool {

    public void Draw (ushort id, Vector2 start, Vector2 end, List<GameObject> added, List<GameObject> removed, int option) {
        List<Vector2> blocks = new List<Vector2>();
        if (Input.GetKey(KeyCode.LeftShift)) {
            blocks = blocksBetweenPointsSnapped(floorToX(start.x, 16), floorToX(start.y, 16),
            floorToX(end.x, 16), floorToX(end.y, 16));
        } else {
            blocks = blocksBetweenPoints(floorToX(start.x, 16), floorToX(start.y, 16),
            floorToX(end.x, 16), floorToX(end.y, 16));
        }
        
        foreach (Vector2 block in blocks) {
            Collider2D[] cols = Physics2D.OverlapPointAll(new Vector2(block.x + 8, block.y + 8));
            foreach (Collider2D c in cols) {
                if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                    removed.Add(c.gameObject);
                    break;
                }
            }

            if (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)) {
                GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                newBlock.transform.position = new Vector3(block.x, block.y, id);
                newBlock.GetComponent<Block>().SetupBlock(id);
                newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(id), "block", -8, -8);
                newBlock.transform.parent = GameObject.Find("Blocks").transform;

                if (!Input.GetMouseButtonUp(0)) {
                    newBlock.GetComponent<SpriteRenderer>().sortingOrder = 1;
                }

                added.Add(newBlock);
            }
        }
    }

    public static int floorToX(float n, int x) {
        return Mathf.FloorToInt(n / x) * x;
    }

    // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
    // Wikipedia - Where any ol' schmuck can make an edit, and yet the site somehow remains useful.
    public List<Vector2> blocksBetweenPoints(int x0, int y0, int x1, int y1) {
        List<Vector2> blocks = new List<Vector2>();
        float dx = x1 - x0;
        float dy = y1 - y0;
        float deltaError = dy / dx;
        float error = deltaError;

        // Way to turn a neat, generalized algorithm into eight spaghetti-looking use cases, me.
        if (dy >= 0) {
            // Quadrants 1 & 2
            if (dx >= 0) {
                if (deltaError == Mathf.Infinity) {
                    int x = x0;
                    for (int y = y0; y <= y1; y += 16) {
                        blocks.Add(new Vector2(x, y));
                    }
                } else if (deltaError < 1) {
                    // Octal 1
                    int y = y0;
                    for (int x = x0; x <= x1; x += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error >= 8f) {
                            y += 16;
                            error -= 16;
                        }
                    }
                } else {
                    // Octal 2
                    deltaError = dx / dy;
                    error = deltaError;
                    int x = x0;
                    for (int y = y0; y <= y1; y += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error >= 8f) {
                            x += 16;
                            error -= 16;
                        }
                    }
                }
            } else {
                if (deltaError > -1) {
                    // Octal 4
                    error = -deltaError + 16f;
                    int y = y1;
                    for (int x = x1; x <= x0; x += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error < 8f) {
                            y -= 16;
                            error += 16;
                        }
                    }
                } else {
                    // Octal 3
                    deltaError = -dx / dy;
                    error = deltaError;
                    int x = x0;
                    for (int y = y0; y <= y1; y += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error >= 8f) {
                            x -= 16;
                            error -= 16;
                        }
                    }
                }
            }
        } else {
            //Quadrants 3 & 4
            if (dx >= 0) {
                if (deltaError == Mathf.NegativeInfinity) {
                    int x = x0;
                    for (int y = y0; y >= y1; y -= 16) {
                        blocks.Add(new Vector2(x, y));
                    }
                } else if (deltaError > -1) {
                    // Octal 8
                    error = -deltaError + 16f;
                    int y = y1;
                    for (int x = x1; x >= x0; x -= 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error < 8f) {
                            y += 16;
                            error += 16;
                        }
                    }
                } else {
                    // Octal 7
                    deltaError = dx / dy;
                    error = -deltaError + 16f;
                    int x = x1;
                    for (int y = y1; y <= y0; y += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error < 8f) {
                            x -= 16;
                            error += 16;
                        }
                    }
                }
            } else {
                if (deltaError < 1) {
                    // Octal 5
                    error = -deltaError;
                    int y = y1;
                    for (int x = x1; x <= x0; x += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error >= 8f) {
                            y += 16;
                            error -= 16;
                        }
                    }
                } else {
                    // Octal 6
                    deltaError = -dx / dy;
                    error = -deltaError + 16f;
                    int x = x1;
                    for (int y = y1; y <= y0; y += 16) {
                        blocks.Add(new Vector2(x, y));
                        error += deltaError * 16;
                        if (error < 8f) {
                            x += 16;
                            error += 16;
                        }
                    }
                }
            }
        }

        return blocks;
    }

    public List<Vector2> blocksBetweenPointsSnapped(int x0, int y0, int x1, int y1) {
        List<Vector2> blocks = new List<Vector2>();
        float dx = x1 - x0;
        float dy = y1 - y0;
        float deltaError = dy / dx;

        if (Mathf.Abs(deltaError) <= 0.5f) {
            int y = y0;

            if (dx > 0) {
                for (int x = x0; x <= x1; x += 16) {
                    blocks.Add(new Vector2(x, y));
                }
            } else {
                for (int x = x0; x >= x1; x -= 16) {
                    blocks.Add(new Vector2(x, y));
                }
            }
        } else if (Mathf.Abs(deltaError) >= 2.0f) {
            int x = x0;

            if (dy > 0) {
                for (int y = y0; y <= y1; y += 16) {
                    blocks.Add(new Vector2(x, y));
                }
            } else {
                for (int y = y0; y >= y1; y -= 16) {
                    blocks.Add(new Vector2(x, y));
                }
            }
        } else {
            int x = x0;
            int y = y0;
            if (dx > 0 && dy > 0) {
                for (; x <= x1; x += 16, y += 16) {
                    blocks.Add(new Vector2(x, y));
                }
            } else if (dx < 0 && dy > 0) {
                for (; x >= x1; x -= 16, y += 16) {
                    blocks.Add(new Vector2(x, y));
                }
            } else if (dx > 0 && dy < 0) {
                for (; x <= x1; x += 16, y -= 16) {
                    blocks.Add(new Vector2(x, y));
                }
            } else if (dx < 0 && dy < 0) {
                for (; x >= x1; x -= 16, y -= 16) {
                    blocks.Add(new Vector2(x, y));
                }
            }
        }

        return blocks;
    }
}
