﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeTool : Tool {

    public void Draw(ushort id, Vector2 start, Vector2 end, List<GameObject> added, List<GameObject> removed, int option) {
        List<Vector2> blocks = new List<Vector2>();

        bool fill = (option == 1 ? true : false);

        int floorStartX = floorToX(start.x, 16);
        int floorEndX = floorToX(end.x, 16);
        int floorStartY = floorToX(start.y, 16);
        int floorEndY = floorToX(end.y, 16);

        if (Input.GetKey(KeyCode.LeftShift)) {
            if (end.x > start.x) {
                if (end.y > start.y) {
                    if (end.x - start.x > end.y - start.y) {
                        floorEndX = floorToX(start.x, 16) + floorToX(end.y, 16) - floorToX(start.y, 16);
                        floorEndY = floorToX(end.y, 16);
                    } else {
                        floorEndX = floorToX(end.x, 16);
                        floorEndY = floorToX(start.y, 16) + floorToX(end.x, 16) - floorToX(start.x, 16);
                    }
                } else {
                    if (end.x - start.x > start.y - end.y) {
                        floorEndX = floorToX(start.x, 16) + floorToX(start.y, 16) - floorToX(end.y, 16);
                        floorEndY = floorToX(end.y, 16);
                    } else {
                        floorEndX = floorToX(end.x, 16);
                        floorEndY = floorToX(start.y, 16) - floorToX(end.x, 16) + floorToX(start.x, 16);
                    }
                }
            } else {
                if (end.y > start.y) {
                    if (start.x - end.x > end.y - start.y) {
                        floorEndX = floorToX(start.x, 16) - floorToX(end.y, 16) + floorToX(start.y, 16);
                        floorEndY = floorToX(end.y, 16);
                    } else {
                        floorEndX = floorToX(end.x, 16);
                        floorEndY = floorToX(start.y, 16) + floorToX(start.x, 16) - floorToX(end.x, 16);
                    }
                } else {
                    if (start.x - end.x > start.y - end.y) {
                        floorEndX = floorToX(start.x, 16) - floorToX(start.y, 16) + floorToX(end.y, 16);
                        floorEndY = floorToX(end.y, 16);
                    } else {
                        floorEndX = floorToX(end.x, 16);
                        floorEndY = floorToX(start.y, 16) + floorToX(end.x, 16) - floorToX(start.x, 16);
                    }
                }
            }
        }

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButton(1) || Input.GetMouseButtonUp(1) || !fill) {
            GameObject fakeFill = GameObject.Find("FakeFill");
            if (fakeFill != null)
                GameObject.Destroy(fakeFill);

            blocks = rectangleFromPoints(floorStartX, floorStartY, floorEndX, floorEndY, fill);
        } else {
            // Performance save by pretending to draw every Blocks with Fill mode, but in reality it's simply tiling the texture on a flat plane.
            // The trade-off comes from letting go of the mouse, confirming that the range should be filled with Blocks.
            GameObject fakeFill = GameObject.Find("FakeFill");
            if (fakeFill == null) {
                fakeFill = GameObject.CreatePrimitive(PrimitiveType.Plane);
                fakeFill.name = "FakeFill";
                fakeFill.GetComponent<MeshCollider>().enabled = false;
                fakeFill.GetComponent<MeshRenderer>().receiveShadows = false;
                fakeFill.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                fakeFill.GetComponent<MeshRenderer>().sortingLayerName = "Foreground";
                fakeFill.GetComponent<MeshRenderer>().sortingOrder = 1;
                fakeFill.transform.localRotation = Quaternion.Euler(90, 0, 180);
                Material mat = new Material(Resources.Load<Shader>("Unlit-Alpha"));
                mat.mainTexture = Texture2DImporter.LoadBlockSprite(BlockID.GetTextureLocation(id)).texture;
                fakeFill.GetComponent<MeshRenderer>().material = mat;
            }

            int countX = (floorEndX > floorStartX ? (floorEndX - floorStartX) / 16 + 1 : (floorStartX - floorEndX) / 16 + 1);
            int countY = (floorEndY > floorStartY ? (floorEndY - floorStartY) / 16 + 1 : (floorStartY - floorEndY) / 16 + 1);

            float startX = floorStartX;
            float startY = floorStartY;
            float endX = floorEndX;
            float endY = floorEndY;

            if (floorEndX >= floorStartX) {
                endX += 16;
            } else {
                startX = floorEndX;
                endX = floorStartX + 16;
            }

            if (end.y >= start.y) {
                endY += 16;
            } else {
                startY = floorEndY;
                endY = floorStartY + 16;
            }

            fakeFill.transform.localPosition = new Vector3((startX + endX) / 2f, (startY + endY) / 2f, fakeFill.transform.localPosition.z);

            fakeFill.transform.localScale = new Vector3(1.6f * countX, 1, 1.6f * countY);
            fakeFill.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(countX, countY);
            fakeFill.GetComponent<MeshRenderer>().material.mainTexture = Texture2DImporter.LoadBlockSprite(BlockID.GetTextureLocation(id)).texture;
            fakeFill.GetComponent<MeshRenderer>().material.mainTexture.wrapMode = TextureWrapMode.Repeat;
        }

        foreach (Vector2 block in blocks) {
            Collider2D[] cols = Physics2D.OverlapPointAll(new Vector2(block.x + 8, block.y + 8));
            foreach (Collider2D c in cols) {
                if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                    removed.Add(c.gameObject);
                    break;
                }
            }
            if (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)) {
                GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                newBlock.transform.position = new Vector3(block.x, block.y, id);
                newBlock.GetComponent<Block>().SetupBlock(id);
                newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(id), "ghost", -8, -8);
                newBlock.transform.parent = GameObject.Find("Blocks").transform;

                if (!Input.GetMouseButtonUp(0)) {
                    newBlock.GetComponent<SpriteRenderer>().sortingOrder = 1;
                }

                added.Add(newBlock);
            }
        }
    }

    public static int floorToX(float n, int x) {
        return Mathf.FloorToInt(n / x) * x;
    }

    private List<Vector2> rectangleFromPoints(int x0, int y0, int x1, int y1, bool fill = false) {
        List<Vector2> blocks = new List<Vector2>();

        int xStart = (x0 < x1 ? x0 : x1);
        int xEnd = (x0 < x1 ? x1 : x0);
        int yStart = (y0 < y1 ? y0 : y1);
        int yEnd = (y0 < y1 ? y1 : y0);

        for (int x = xStart; x <= xEnd; x += 16) {
            for (int y = yStart; y <= yEnd; y += 16) {
                if (x == xStart || x == xEnd || y == yStart || y == yEnd || fill) {
                    blocks.Add(new Vector2(x, y));
                }
            }
        }

        return blocks;
    }
}
