﻿using UnityEngine;
using System.Collections.Generic;

public enum BlockType { Background, Foreground, Solid, Ladder, Water };
public enum ModType { None, Kill };

public class Block : MonoBehaviour {
    
    public ushort blockID;
    public BlockType blockType;
    public ModType modType;
    
    public void SetupBlock(ushort ID) {
        if (ID == 0) {
            Debug.LogError("Invalid ID 0! (Blocks should not use this ID!)");
        } else if (!BlockID.isValidID(ID)) {
            Debug.LogError("Invalid ID " + ID + "!");
            ID = 0;
        }
        blockID = ID;
        Sprite spr = Texture2DImporter.LoadBlockSprite(BlockID.GetTextureLocation(ID));
        GetComponent<SpriteRenderer>().sprite = spr;

        blockType = BlockID.GetBlockType(ID);
        modType = BlockID.GetModType(ID);
    }

    public bool CheckTopLadder() {
        Collider2D top;//, bottom;
        top = Physics2D.OverlapPoint(new Vector2(transform.position.x + 8, transform.position.y + 24), 1 << LayerMask.NameToLayer("Block"));
        return (top == null || (top.GetComponent<Block>().blockType != BlockType.Solid && top.GetComponent<Block>().blockType != BlockType.Ladder));
    }

    public bool CheckLadderBeneath() {
        Collider2D bottom;
        bottom = Physics2D.OverlapPoint(new Vector2(transform.position.x + 8, transform.position.y - 8), 1 << LayerMask.NameToLayer("Block"));
        return (bottom != null && bottom.GetComponent<Block>().blockType == BlockType.Ladder);
    }

    public bool CheckSolidUnderneath() {
        Collider2D bottom;
        bottom = Physics2D.OverlapPoint(new Vector2(transform.position.x + 8, transform.position.y - 8), 1 << LayerMask.NameToLayer("Block"));
        return (bottom != null && bottom.GetComponent<Block>().blockType == BlockType.Solid);
    }
}
