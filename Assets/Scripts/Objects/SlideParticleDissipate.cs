﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideParticleDissipate : MonoBehaviour {

    private SpriteRenderer sprRender;
    private int timer;

	void Start () {
        sprRender = GetComponent<SpriteRenderer>();
	}
	
	void FixedUpdate () {
        if (timer < 8) {
            sprRender.sprite = Texture2DImporter.LoadTexture2DToSprite("FX/slide0", "slide0");
        } else if (timer < 16) {
            sprRender.sprite = Texture2DImporter.LoadTexture2DToSprite("FX/slide1", "slide1");
        } else if (timer < 17) {
            sprRender.sprite = Texture2DImporter.LoadTexture2DToSprite("FX/slide2", "slide2");
        } else {
            Destroy(gameObject);
        }
        timer++;
	}
}
