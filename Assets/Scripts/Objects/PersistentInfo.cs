﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LevelLoadedType { PLAY, BUILD };

public class PersistentInfo : MonoBehaviour {

    public string fileNameForNextPlayLoad;
    public LevelLoadedType levelLoadedType;
    public bool newBuildLevel;

    void Awake() {
        DontDestroyOnLoad(gameObject);

        GameObject other = GameObject.Find("PersistentInfo");
        if (other != null && other != gameObject)
            Destroy(gameObject);
    }
}
