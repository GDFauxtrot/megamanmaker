﻿using UnityEngine;
using System.Collections;

// Why muddle up PlayerController code with booleans when you can
// clean it up with a simple class that's easy to understand?

public class Keypad : MonoBehaviour {

    [HideInInspector]
    public bool right,
                down,
                left,
                up,
                jump,
                jumpDown,
                shoot,
                shootDown;

    /** Helper functions **/

    public bool OnlyLeft() {
        return left && !right;
    }

    public bool OnlyRight() {
        return right && !left;
    }

    public bool OnlyUp() {
        return up && !down;
    }

    public bool OnlyDown() {
        return down && !up;
    }

    /// <summary>
    /// Returns true when only left or right is held, false otherwise.
    /// Useful for determining if the player is going to move or not.
    /// </summary>
    public bool LeftOrRight() {
        return left ^ right;
    }

    /// <summary>
    /// Returns true when only up or down is held, false otherwise.
    /// </summary>
    public bool UpOrDown() {
        return up ^ down;
    }
}
