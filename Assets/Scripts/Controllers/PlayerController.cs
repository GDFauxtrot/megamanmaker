﻿using UnityEngine;
using UnityEngine.EventSystems;

public enum State { IDLE, RUN_START, RUN, RUN_END, MIDAIR, LAND, SLIDE, CLIMB, HURT };

public class PlayerController : MonoBehaviour {

    private Keypad keypad;
    private PlayerAnimator animator;

	// Pixel snap is Mathf.floor, which is the same snap used on the NES (easier to calc than nearest whole number)
    public bool enablePixelSnapping;

	// Editable controls
	public KeyCode keyRight;
	public KeyCode keyLeft;
	public KeyCode keyUp;
	public KeyCode keyDown;
	public KeyCode keyJump;
    public KeyCode keyJumpAlt;
	public KeyCode keyShoot;
    public KeyCode keyShootAlt;

    // Movement constants
    public const float RUNSTART_SPD = 0.25f;
    public const float RUN_SPD = 4 / 3f;
    public const float CLIMB_SPD = 3 / 4f;
    public const float RUNSTOP_SPD = 0.5f;
    public const float INIT_JUMP_VEL = 5f;
    public const float MID_JUMP_VEL = 1.125f;
    public const float GRAVITY = 0.25f;
	public const float SLIDE_SPD = 2.5f;
    public const float MAX_FALL_SPD = -12f;

	// Mega Man jump height:	50 units
    // Mega Man jump pos:  		y = -1/8 * t^2 + 50
    // Derivative (velocity):   vy = -1/4 * t
    // t = 60 * seconds elapsed (# of elapsed frames at 60fps)

    // Sprites face left by default - flip sprite when facing right
    public bool isFacingRight;

    // We'll designate "speed" as the change in position in each fixed frame
    private Vector2 speed;
    // Y velocity used to figure out speed due to gravity
    private float yVelocity;

    // Holds our un-snapped X/Y position (use this instead of transform.position!)
    private Vector2 truePosition;

    // A State enum will help us determine and self-document Mega Man behavior
    private State state;
    
    // Shooting makes MM change sprite to a shooting one, not a state change
    private int shootCounter;
    // Some states depend on timers
    private int stateCounter;

    public bool paused;

	private bool slideDirectionRight;
	private bool slidingUnder;
	private bool slidPastEdge;
    private bool setClimbing;
    private bool climbingAboveTopLadder;
    private bool lastShotRight;
    private bool justAfterShootCounter;

    void Start () {
        keypad = GetComponent<Keypad>();
        animator = GetComponent<PlayerAnimator>();
        truePosition = new Vector2(transform.position.x, transform.position.y);
    }

	void Update() {
        if (paused)
            return;

        // To make sure we always catch input, we catch input in Update
        // and reset GetKeyDown flags after they were intercepted in FixedUpdate

        keypad.left = Input.GetKey(keyLeft);
        keypad.right = Input.GetKey(keyRight);
        keypad.up = Input.GetKey(keyUp);
        keypad.down = Input.GetKey(keyDown);

        keypad.jump = Input.GetKey(keyJump) || Input.GetKey(keyJumpAlt);
        keypad.shoot = Input.GetKey(keyShoot) || Input.GetKey(keyShootAlt);

        if (!keypad.jumpDown)
            keypad.jumpDown = Input.GetKeyDown(keyJump) || Input.GetKeyDown(keyJumpAlt);
        if (!keypad.shootDown)
            keypad.shootDown = Input.GetKeyDown(keyShoot) || Input.GetKeyDown(keyShootAlt);

        if (keypad.OnlyLeft() && state != State.CLIMB) {
            isFacingRight = false;
        } else if (keypad.OnlyRight() && state != State.CLIMB) {
            isFacingRight = true;
        }
	}

    void FixedUpdate() {
        // Update shoot timer
        if (shootCounter > 0) {
            shootCounter--;
            if (shootCounter == 0)
                justAfterShootCounter = true;
            if (state == State.CLIMB)
                isFacingRight = lastShotRight;
        } else {
            if (state == State.CLIMB && justAfterShootCounter) {
                isFacingRight = !lastShotRight;
                justAfterShootCounter = false;
            }
        }
        if (keypad.shootDown && state != State.SLIDE) {
            shootCounter = 15;
            if (state == State.CLIMB) {
                if (keypad.OnlyRight())
                    lastShotRight = true;
                else if (keypad.OnlyLeft())
                    lastShotRight = false;
                isFacingRight = lastShotRight;
            }

        }

        UpdatePlayerState();

        if (keypad.OnlyDown() && keypad.jumpDown &&
            (state == State.IDLE || state == State.RUN_START || state == State.RUN || state == State.RUN_END)) {
            state = State.SLIDE;
            slidPastEdge = false;
            stateCounter = 25;
            shootCounter = 0;
            animator.ResetAnimationTimer();
            slideDirectionRight = isFacingRight;
            GameObject slideParticle = Instantiate(Resources.Load<GameObject>("SlideParticle"));
            if (isFacingRight) {
                slideParticle.GetComponent<SpriteRenderer>().flipX = true;
                slideParticle.transform.position = new Vector3(truePosition.x - 9, truePosition.y - 8,
                    slideParticle.transform.position.z);
            } else {
                slideParticle.GetComponent<SpriteRenderer>().flipX = false;
                slideParticle.transform.position = new Vector3(truePosition.x + 9, truePosition.y - 8,
                    slideParticle.transform.position.z);
            }
        }

        // POST-MOVEMENT ADJUSTMENTS
        truePosition = new Vector2(truePosition.x + speed.x, truePosition.y + speed.y);

        // COLLISION DETECTION
        CheckCollision();

        // Hard borders until map size calculations and scrolling are implemented
        if (truePosition.x < 16f) {
            truePosition = new Vector2(16f, truePosition.y);
        }
        if (truePosition.x > Camera.main.GetComponent<FollowCam>().worldMax.x - 16f) {
            truePosition = new Vector2(Camera.main.GetComponent<FollowCam>().worldMax.x - 16f, truePosition.y);
        }

        if (enablePixelSnapping) {
            transform.position = new Vector3(Mathf.Ceil(truePosition.x), Mathf.Ceil(truePosition.y), transform.position.z);
        } else {
            transform.position = new Vector3(truePosition.x, truePosition.y, transform.position.z);
        }

        // Reset GetKeyDown input flags
        keypad.jumpDown = false;
        keypad.shootDown = false;
    }

	private void CheckCollision() {
        // COLLISION OVERVIEW:
        // We define collisions as moments of overlap with Block objects. (Enemies/Items are separate)
        // Collisions are detected immediately before we apply truePosition to transform.position.
        // When a collision is detected, we alter truePosition appropriately.

        BoxCollider2D col2D = GetComponent<BoxCollider2D>();
        float xx_min = col2D.size.x / 2 - col2D.offset.x;
        float xx_max = col2D.size.x / 2 + col2D.offset.x;
        float yy_min = col2D.size.y / 2 - col2D.offset.y;
        float yy_max = col2D.size.y / 2 + col2D.offset.y;

        // Start by identifying all Blocks in which MM's collider is crossing into.

        LayerMask mask = 1 << LayerMask.NameToLayer("Block");
        
		Collider2D[] touching = Physics2D.OverlapAreaAll(
			new Vector2(truePosition.x - xx_min, truePosition.y - yy_min),
			new Vector2(truePosition.x + xx_max, truePosition.y + yy_max), mask.value);

        // Sort by nearest blocks to make sure the nearest collisions are done first
		System.Array.Sort(touching,
            delegate(Collider2D col1, Collider2D col2) {
                return Vector2.Distance(truePosition, col1.transform.position).CompareTo(
                    Vector2.Distance(truePosition, col2.transform.position));
		    });

		if (state != State.MIDAIR && touching.Length == 0) {
			// We're not touching anything but we're not midair... we must've walked off a ledge.
			state = State.MIDAIR;
			speed = new Vector2(speed.x, 0f);
			yVelocity = 0f;
		}

        setClimbing = false;
        bool goMidair = true;
		bool stopSlideFall = state == State.SLIDE;
        bool stopSlideStand = state == State.SLIDE;
		bool checkSlide = false;
        bool standTopOfLadder = false;
        bool standLadderGrab = false;
        bool touchingLadder = false;
        bool touchingLadderError = false;
        float ladderX = 0;
        climbingAboveTopLadder = false;
        float edgeDisplacementLeft = 0f;
		float edgeDisplacementRight = 0f;
        
		for (int i = 0; i < touching.Length; i++) {
			Block block = touching[i].GetComponent<Block>();

			if (block == null) {
				throw new MissingComponentException("Object on Block layer is missing a Block component! ("
					+ touching[i].transform.position.x + ", " + touching[i].transform.position.y + ")");
			}

            if (block.blockType == BlockType.Ladder) {
                if (truePosition.x >= block.transform.localPosition.x && truePosition.x <= block.transform.localPosition.x+16) {
                    //Debug.Log(block.transform.localPosition.x + " " + truePosition.x);
                    if (truePosition.y >= block.transform.position.y) {
                        touchingLadder = true;
                    } else {
                        if (state == State.CLIMB && !block.CheckLadderBeneath() && !block.CheckSolidUnderneath()) {
                            touchingLadder = false;
                            touchingLadderError = true;
                        }
                    }
                    if (block.CheckTopLadder()) {
                        if (truePosition.y > block.transform.position.y + 4)
                            climbingAboveTopLadder = true;
                        if (state == State.CLIMB && keypad.OnlyUp() && truePosition.y > block.transform.position.y + 12) {
                            Collider2D top;
                            top = Physics2D.OverlapPoint(new Vector2(block.transform.position.x + 8, block.transform.position.y + 40), 1 << LayerMask.NameToLayer("Block"));
                            if (top == null || top.GetComponent<Block>().blockType != BlockType.Solid) {
                                truePosition = new Vector2(truePosition.x, block.transform.position.y + 16 + col2D.size.y / 2);
                                standTopOfLadder = true;
                                break;
                            } else {
                                speed = Vector2.zero;
                                yVelocity = 0f;
                            }
                            
                        }
                        float displacementFromBottom = touching[i].bounds.max.y - (truePosition.y - yy_min);
                        float displacementFromTop = (truePosition.y + yy_max) - touching[i].bounds.min.y;
                        float displacementFromLeft = touching[i].bounds.max.x - (truePosition.x - xx_min);
                        float displacementFromRight = (truePosition.x + xx_max) - touching[i].bounds.min.x;

                        bool onLeftEdge = displacementFromLeft <= SLIDE_SPD && displacementFromRight >= 32f - SLIDE_SPD;
                        bool onRightEdge = displacementFromLeft >= 32f - SLIDE_SPD && displacementFromRight <= SLIDE_SPD;

                        float[] displacements = { displacementFromLeft, displacementFromRight, displacementFromTop, displacementFromBottom };

                        if (Mathf.Min(displacements) == displacementFromBottom) {
                            goMidair = false;
                            if (!(onLeftEdge || onRightEdge)) {
                                stopSlideFall = false;
                            } else {
                                edgeDisplacementLeft = displacementFromLeft;
                                edgeDisplacementRight = displacementFromRight;
                            }
                            if (state == State.MIDAIR && speed.y <= 0) {
                                speed = new Vector2(speed.x, 0f);
                                yVelocity = 0f;
                                Collider2D top;
                                top = Physics2D.OverlapPoint(new Vector2(block.transform.position.x + 8, block.transform.position.y + 40), 1 << LayerMask.NameToLayer("Block"));
                                if (top == null || top.GetComponent<Block>().blockType != BlockType.Solid) {
                                    truePosition = new Vector2(truePosition.x, truePosition.y + displacementFromBottom);
                                    if (keypad.LeftOrRight()) {
                                        state = State.RUN;
                                        stateCounter = 0;
                                        animator.ResetAnimationTimer();
                                    } else {
                                        state = State.LAND;
                                        stateCounter = 3;
                                        animator.ResetAnimationTimer();
                                    }
                                } else {
                                    truePosition = new Vector2(truePosition.x, truePosition.y + displacementFromBottom - 16);
                                }
                            }
                            if (keypad.OnlyDown() && !block.CheckSolidUnderneath() &&
                                    (state == State.IDLE || state == State.RUN_START || state == State.RUN ||
                                    state == State.RUN_END || state == State.LAND || state == State.SLIDE)) {
                                setClimbing = true;
                                truePosition = new Vector2(block.transform.position.x + 8, block.transform.position.y + 12);
                                state = State.CLIMB;
                                shootCounter = 0;
                                break;
                            }
                        } else {
                            if (state == State.MIDAIR && keypad.UpOrDown() && truePosition.y > block.transform.position.y) {
                                setClimbing = true;
                                truePosition = new Vector2(block.transform.position.x + 8, truePosition.y);
                                state = State.CLIMB;
                                shootCounter = 0;
                                break;
                            }
                        }
                    } else {
                        if ((state == State.MIDAIR && keypad.UpOrDown()) || (keypad.UpOrDown() &&
                            (state == State.IDLE || state == State.RUN_START || state == State.RUN || state == State.RUN_END || state == State.LAND || state == State.SLIDE))) {
                            if (keypad.OnlyDown() && standLadderGrab && block.CheckSolidUnderneath())
                                standLadderGrab = false;
                            if (keypad.OnlyUp() && truePosition.y >= block.transform.position.y) {
                                speed = Vector2.zero;
                                setClimbing = true;
                                state = State.CLIMB;
                                shootCounter = 0;
                                truePosition = new Vector2(block.transform.position.x + 8, truePosition.y);
                                break;
                            } else if (keypad.OnlyDown() && block.CheckLadderBeneath()) {
                                ladderX = block.transform.position.x;
                                standLadderGrab = true;
                                //break;
                            }
                        }
                    }
                } else {
                    if (state == State.CLIMB) {
                        touchingLadderError = true;
                    }
                }
            } else if (block.blockType == BlockType.Solid) {
				float displacementFromBottom = touching[i].bounds.max.y - (truePosition.y - yy_min);
				float displacementFromTop = (truePosition.y + yy_max) - touching[i].bounds.min.y;
				float displacementFromLeft = touching[i].bounds.max.x - (truePosition.x - xx_min);
				float displacementFromRight = (truePosition.x + xx_max) - touching[i].bounds.min.x;

				float[] displacements = {displacementFromLeft, displacementFromRight, displacementFromTop, displacementFromBottom};

				bool onLeftEdge = displacementFromLeft <= SLIDE_SPD && displacementFromRight >= 32f-SLIDE_SPD;
				bool onRightEdge = displacementFromLeft >= 32f-SLIDE_SPD && displacementFromRight <= SLIDE_SPD;

                if (Mathf.Min(displacements) == displacementFromLeft) {
                    //Debug.Log("LEFT");
                    if (!setClimbing) {
                        if (displacementFromBottom + speed.y < 0 && touching.Length == 1 && displacementFromLeft != 0f) {
                            // This is a strange one -- certain-distanced falls will result in a bottom collision 
                            // looking like a side collision when landing on a block edge. This condition seems to be the solution,
                            // though I'm willing to bet it will create problems later on.
                            speed = new Vector2(speed.x, 0f);
                            yVelocity = 0f;
                            truePosition = new Vector2(truePosition.x, truePosition.y + displacementFromBottom);
                            if (keypad.LeftOrRight()) {
                                state = State.RUN;
                                stateCounter = 0;
                                animator.ResetAnimationTimer();
                            } else {
                                state = State.LAND;
                                stateCounter = 3;
                                animator.ResetAnimationTimer();
                            }
                        } else {
                            truePosition = new Vector2(truePosition.x + displacementFromLeft, truePosition.y);
                        }
                    }
				} else if (Mathf.Min(displacements) == displacementFromRight) {
                    //Debug.Log("RIGHT");
                    if (!setClimbing) {
                        if (displacementFromBottom + speed.y < 0 && touching.Length == 1 && displacementFromRight != 0f) {
                            // Same as displacementFromLeft
                            speed = new Vector2(speed.x, 0f);
                            yVelocity = 0f;
                            truePosition = new Vector2(truePosition.x, truePosition.y + displacementFromBottom);
                            if (keypad.LeftOrRight()) {
                                state = State.RUN;
                                stateCounter = 0;
                                animator.ResetAnimationTimer();
                            } else {
                                state = State.LAND;
                                stateCounter = 3;
                                animator.ResetAnimationTimer();
                            }
                        } else {
                            Collider2D bottom;
                            bottom = Physics2D.OverlapPoint(new Vector2(block.transform.position.x - 8, block.transform.position.y + 8), 1 << LayerMask.NameToLayer("Block"));
                            if (!(bottom != null && bottom.GetComponent<Block>().blockType == BlockType.Solid))
                                truePosition = new Vector2(truePosition.x - displacementFromRight, truePosition.y);
                        }
                    }
				} else if (Mathf.Min(displacements) == displacementFromTop) {
					//Debug.Log("TOP");
					checkSlide = true;
                    // For stateCounter condition: if a player starts sliding right on a slide gap,
                    // the collider will shift ahead 2 units and the resulting initial displacement will
                    // immediately be 4.5, skipping the slidPastEdge condition and resulting in a
                    // skipped stopSlideStand gap. We check for this collider jump anomaly with the
                    // stateCounter and a 2-units-larger edge range check.
                    if (!slideDirectionRight && onLeftEdge || slideDirectionRight && onRightEdge || stateCounter == 24 &&
                        (!slideDirectionRight && displacementFromLeft <= SLIDE_SPD + 2
                        || slideDirectionRight && displacementFromRight <= SLIDE_SPD + 2)) {
                        if (!slidPastEdge) {
                            slidPastEdge = true;
                            stopSlideStand = false;
                        } else {
                            edgeDisplacementLeft = displacementFromLeft;
                            edgeDisplacementRight = displacementFromRight;
                        }
                    } else {
                        stopSlideStand = false;
                    }
                    if (state == State.MIDAIR && speed.y > 0) {
                        // 2 unit edge grace for making jumping up into 1-block gaps easier
                        if (displacementFromLeft < 2f)
                            truePosition = new Vector2(truePosition.x + displacementFromLeft, truePosition.y);
                        else if (displacementFromRight < 2f)
                            truePosition = new Vector2(truePosition.x - displacementFromRight, truePosition.y);
                        else {
                            speed = new Vector2(speed.x, 0f);
                            yVelocity = 0;
                            truePosition = new Vector2(truePosition.x, truePosition.y - displacementFromTop);
                        }
					}
                    if (state == State.CLIMB) {
                        truePosition = new Vector2(truePosition.x, truePosition.y - displacementFromTop);
                        animator.ResetAnimationTimer();
                        if (keypad.OnlyUp())
                            speed = Vector2.zero;
                    }
				} else if (Mathf.Min(displacements) == displacementFromBottom) {
                    //Debug.Log("BOTTOM");
                    goMidair = false;
					if (!(onLeftEdge || onRightEdge)) {
						stopSlideFall = false;
					} else {
						edgeDisplacementLeft = displacementFromLeft;
						edgeDisplacementRight = displacementFromRight;
					}
					if ((state == State.MIDAIR && speed.y <= 0) || (state == State.CLIMB && displacementFromBottom != 0f)) {
						speed = new Vector2(speed.x, 0f);
						yVelocity = 0f;
						truePosition = new Vector2(truePosition.x, truePosition.y + displacementFromBottom);
						if (keypad.LeftOrRight()) {
							state = State.RUN;
							stateCounter = 0;
							animator.ResetAnimationTimer();
						} else {
							state = State.LAND;
							stateCounter = 3;
							animator.ResetAnimationTimer();
						}
						continue;
					}
				}
			}
        }
		slidingUnder = checkSlide;

        if (state == State.CLIMB && !touchingLadder && touchingLadderError) {
            state = State.MIDAIR;
            speed = Vector2.zero;
            yVelocity = 0f;
        }
        if (standLadderGrab) {
            setClimbing = true;
            truePosition = new Vector2(ladderX + 8, truePosition.y);
            state = State.CLIMB;
            shootCounter = 0;
        }
        if (touching.Length > 0) {
			if (stopSlideFall) {
				state = State.MIDAIR;
				if (edgeDisplacementLeft < edgeDisplacementRight) {
					truePosition = new Vector2(truePosition.x + edgeDisplacementLeft, truePosition.y - 4f);
				} else {
					truePosition = new Vector2(truePosition.x - edgeDisplacementRight, truePosition.y - 4f);
				}
			}
            if (stopSlideStand && checkSlide) {
				state = State.IDLE;
                slidPastEdge = false;
				if (slideDirectionRight) {
					truePosition = new Vector2(truePosition.x - edgeDisplacementRight + 4.5f, truePosition.y);
				} else {
					truePosition = new Vector2(truePosition.x + edgeDisplacementLeft - 4.5f, truePosition.y);
				}
			}
			if (goMidair && state != State.CLIMB) {
				// We collided but none of it was from the bottom... we should be midair
				state = State.MIDAIR;
			}
		}
        if (standTopOfLadder) {
            state = State.IDLE;
            yVelocity = 0f;
        }
    }

	private void UpdatePlayerState() {
		if (keypad.jumpDown && state != State.MIDAIR && !keypad.OnlyDown() && !slidingUnder) {
			if (state == State.SLIDE) {
				state = State.IDLE;
				stateCounter = 0;
				animator.ResetAnimationTimer();
			} else {
                if (!(state == State.CLIMB && keypad.UpOrDown())) {
                    if (state == State.CLIMB)
                        yVelocity = 0f;
                    else
                        yVelocity = INIT_JUMP_VEL;
                    state = State.MIDAIR;
                }
                
			}
		}

		switch (state) {
		    case State.IDLE:
		
			    speed = Vector2.zero;
			    if (keypad.LeftOrRight()) {
				    state = State.RUN_START;
				    // Set up timers for RUN_START
				    stateCounter = 9;
				    shootCounter = 0;
				    animator.ResetAnimationTimer();
			    }
			    break;

		    case State.RUN_START:
		
			    if (keypad.LeftOrRight()) {
				    if (isFacingRight) {
					    speed = new Vector2(RUNSTART_SPD, speed.y);
				    } else {
					    speed = new Vector2(-RUNSTART_SPD, speed.y);
				    }
				    stateCounter--;
				    if (stateCounter <= 0) {
					    state = State.RUN;
					    stateCounter = 0;
					    animator.ResetAnimationTimer();
				    }
			    } else {
				    // Reset to IDLE when the player lets go of a direction
				    state = State.IDLE;
				    stateCounter = 0;
				    animator.ResetAnimationTimer();
			    }
			    break;

		    case State.RUN:
		
			    if (isFacingRight) {
				    speed = new Vector2(RUN_SPD, speed.y);
			    } else {
				    speed = new Vector2(-RUN_SPD, speed.y);
			    }
			    // State stays as RUN until the player stops moving
			    if (!keypad.LeftOrRight()) {
				    state = State.RUN_END;
				    // Set up timer for RUN_END
				    stateCounter = 15;
				    animator.ResetAnimationTimer();
			    }
			    break;

		    case State.RUN_END:
		
			    if (isFacingRight) {
				    speed = new Vector2(RUNSTOP_SPD, speed.y);
			    } else {
				    speed = new Vector2(-RUNSTOP_SPD, speed.y);
			    }
			    if (!keypad.LeftOrRight()) {
				    stateCounter--;
				    if (stateCounter <= 0) {
					    state = State.IDLE;
					    stateCounter = 0;
					    animator.ResetAnimationTimer();
				    }
			    } else {
				    // Revert to RUN_START if the player interrupts
				    // (Note: counter resets to 8 in this case on NES)
				    state = State.RUN_START;
				    stateCounter = 8;
				    animator.ResetAnimationTimer();
			    }
			    break;

		    case State.MIDAIR:
		
			    // yf = yi + vi*t + 0.5*a*t*t (yi is added during transform)
			    var y = yVelocity - 0.5f * GRAVITY;
                if (y < MAX_FALL_SPD)
                    y = MAX_FALL_SPD;
			    if (!keypad.jump && yVelocity > MID_JUMP_VEL) {
				    // Releasing jump causes MM to jump cancel (force velocity to 9/8)
				    yVelocity = MID_JUMP_VEL;
			    } else {
				    // vf = vi + a*t
                    if (y > MAX_FALL_SPD)
				        yVelocity -= GRAVITY;
			    }

			    if (keypad.OnlyRight()) {
				    speed = new Vector2(RUN_SPD, y);
			    } else if (keypad.OnlyLeft()) {
				    speed = new Vector2(-RUN_SPD, y);
			    } else {
				    speed = new Vector2(0f, y);
			    }
			    break;

		    case State.LAND:
		
			    if (!keypad.LeftOrRight()) {
				    stateCounter--;
				    if (stateCounter <= 0) {
					    state = State.IDLE;
					    stateCounter = 0;
					    animator.ResetAnimationTimer();
				    }
			    } else {
				    state = State.RUN_START;
				    stateCounter = 9;
				    shootCounter = 0;
				    animator.ResetAnimationTimer();
			    }
			    break;

		    case State.SLIDE:
			
			    // Cancel slide if opposite direction pressed
			    if (!slidingUnder &&
				    (slideDirectionRight && keypad.OnlyLeft() ||
				    !slideDirectionRight && keypad.OnlyRight())) {
				    stateCounter = 0;
			    }
			    if (stateCounter > 0) {
				    stateCounter--;
				    if (isFacingRight) {
					    speed = new Vector2(SLIDE_SPD, speed.y);
				    } else {
					    speed = new Vector2(-SLIDE_SPD, speed.y);
				    }
			    } else {
				    if (slidingUnder) {
					    if (isFacingRight) {
						    speed = new Vector2(SLIDE_SPD, speed.y);
					    } else {
						    speed = new Vector2(-SLIDE_SPD, speed.y);
					    }
				    } else {
					    if (keypad.LeftOrRight()) {
						    state = State.RUN_START;
						    stateCounter = 9;
					    } else {
						    state = State.IDLE;
						    stateCounter = 0;
					    }
					    animator.ResetAnimationTimer();
				    }
			    }

			    break;

		    case State.CLIMB:
                if (keypad.OnlyUp() && shootCounter == 0)
                    speed = new Vector2(0, CLIMB_SPD);
                else if (keypad.OnlyDown() && shootCounter == 0)
                    speed = new Vector2(0, -CLIMB_SPD);
                else
                    speed = Vector2.zero;
			    break;

		    case State.HURT:
			
			    break;
		}
	}

    public int GetStateCounter() {
        return stateCounter;
    }

    public bool IsShooting() {
        return shootCounter > 0;
    }

    public void SetState(State s) {
        state = s;
    }

    public State GetState() {
        return state;
    }

    public Keypad GetInput() {
        return keypad;
    }

    /// <summary>
    /// Used to force position (setting position & localPosition elsewhere will not work).
    /// Avoid using this function unless it's really necessary (like level spawning).
    /// </summary>
    public void ForceTruePosition(Vector3 pos) {
        truePosition = pos;
    }

    public bool climbingUpTopLadder() {
        return climbingAboveTopLadder;
    }

    public bool animateClimb() {
        return state == State.CLIMB && keypad.UpOrDown();
    }
    public void TogglePixelSnapping() {
        enablePixelSnapping = !enablePixelSnapping;
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
    }
}
