﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum PauseMenuType { Play, Build };

public class GameMenuManager : MonoBehaviour {

    public PauseMenuType pauseType;

    GameObject pausePanel, optionsPanel;
    GameObject savePanel;

    KeyCode pauseKey = KeyCode.Escape;

    bool isPaused;

    GameObject canvas;
    GameObject bgFade;

    void Start () {

        canvas = GameObject.Find("Canvas");

        SetupBackgroundFade();

        if (pauseType == PauseMenuType.Play) {
            SetupPlayPanels();
        } else if (pauseType == PauseMenuType.Build) {
            SetupBuildPanels();
        }
    }
	
	void Update () {
        if (Input.GetKeyDown(pauseKey)) {
            if (pauseType == PauseMenuType.Play) {
                DoPlayPause();
            } else if (pauseType == PauseMenuType.Build) {
                DoBuildPause();
            }
        }
    }

    public void ResumePressed() {
        pausePanel.SetActive(false);
        isPaused = false;

        bgFade.SetActive(false);
        Time.timeScale = 1f;

        if (pauseType == PauseMenuType.Play)
            GameObject.Find("MegaMan").GetComponent<PlayerController>().paused = false;
        else if (pauseType == PauseMenuType.Build)
            Camera.main.GetComponent<EditorController>().paused = false;
    }

    public void OptionsPressed() {
        pausePanel.SetActive(false);
        optionsPanel.SetActive(true);
    }

    public void OptionsReturnPressed() {
        pausePanel.SetActive(true);
        optionsPanel.SetActive(false);
    }

    public void QuitPressed() {
        pausePanel.SetActive(false);
        isPaused = false;
        bgFade.SetActive(false);
        Time.timeScale = 1f;

        if (pauseType == PauseMenuType.Play)
            GameObject.Find("MegaMan").GetComponent<PlayerController>().paused = false;
        else if (pauseType == PauseMenuType.Build)
            Camera.main.GetComponent<EditorController>().paused = false;

        SceneManager.LoadScene("Intro");
    }

    public void SavePressed() {
        if (pauseType == PauseMenuType.Play) {
            Debug.LogError("Should not be in play mode!");
            return;
        }

        pausePanel.SetActive(false);
        isPaused = false;
        bgFade.SetActive(false);
        Time.timeScale = 1f;

        Camera.main.GetComponent<EditorController>().pickPlayerPosAndSave();
        Camera.main.GetComponent<EditorController>().paused = false;
    }

    public void LoadPressed() {
        if (pauseType == PauseMenuType.Play) {
            Debug.LogError("Should not be in play mode!");
            return;
        }


    }

    public void BuildMenuPressed() {
        pausePanel.SetActive(true);
        bgFade.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Save_NameInputChanged(string input) {
        string fileName = SanitizeFileName(input);

        if (fileName.Length == 0) {
            savePanel.transform.Find("FileText").GetComponent<Text>().text = "File name:";
            savePanel.transform.Find("SaveButton").GetComponent<Button>().interactable = false;
        } else {
            savePanel.transform.Find("FileText").GetComponent<Text>().text = "File name: " + fileName + ".mml";
            savePanel.transform.Find("SaveButton").GetComponent<Button>().interactable = true;
        }
    }

    public void Save_SavePressed() {
        string levelName = savePanel.transform.Find("NameInput").GetComponent<InputField>().text;
        string authorName = savePanel.transform.Find("AuthorInput").GetComponent<InputField>().text;
        string levelDescription = savePanel.transform.Find("DescriptionInput").GetComponent<InputField>().text;
        Vector2 spawn = Camera.main.GetComponent<EditorController>().startPosBlockAligned;
        Color top = Camera.main.GetComponent<BackgroundDraw>().colorTop;
        Color bottom = Camera.main.GetComponent<BackgroundDraw>().colorBottom;
        Camera.main.GetComponent<LevelIO>().SaveLevelFile(levelName, authorName, levelDescription, top, bottom, spawn);
    }

    public void Save_CancelPressed() {
        isPaused = false;
        savePanel.SetActive(false);

        bgFade.SetActive(false);
        Time.timeScale = 1f;

        Camera.main.GetComponent<EditorController>().paused = false;
        Camera.main.GetComponent<EditorController>().ResetSaveSpawn();
    }
    
    public void ShowSaveMenu() {
        isPaused = true;
        savePanel.SetActive(true);

        bgFade.SetActive(true);
        Time.timeScale = 0f;

        Camera.main.GetComponent<EditorController>().paused = true;
    }

    private void DoPlayPause() {
        if (isPaused) {
            if (pausePanel.activeSelf == true) {
                isPaused = false;
                pausePanel.SetActive(false);

                bgFade.SetActive(false);
                Time.timeScale = 1f;

                GameObject.Find("MegaMan").GetComponent<PlayerController>().paused = false;
            } else {
                if (optionsPanel.activeSelf == true)
                    optionsPanel.SetActive(false);
                pausePanel.SetActive(true);
            }
        } else {
            isPaused = true;
            pausePanel.SetActive(true);

            bgFade.SetActive(true);
            Time.timeScale = 0f;

            GameObject.Find("MegaMan").GetComponent<PlayerController>().paused = true;
        }
    }

    private void DoBuildPause() {
        if (isPaused) {
            if (pausePanel.activeSelf == true) {
                isPaused = false;
                pausePanel.SetActive(false);

                bgFade.SetActive(false);
                Time.timeScale = 1f;

                Camera.main.GetComponent<EditorController>().paused = false;
            } else {
                if (optionsPanel.activeSelf == true)
                    optionsPanel.SetActive(false);
                pausePanel.SetActive(true);
            }
        } else {
            isPaused = true;
            pausePanel.SetActive(true);

            bgFade.SetActive(true);
            Time.timeScale = 0f;

            Camera.main.GetComponent<EditorController>().paused = true;
        }
    }

    private void SetupPlayPanels() {
        pausePanel = (GameObject) Instantiate(Resources.Load("Pause/PauseMain-Play"));
        optionsPanel = (GameObject) Instantiate(Resources.Load("Pause/PauseOptions"));

        pausePanel.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ResumePressed);
        pausePanel.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(OptionsPressed);
        pausePanel.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(QuitPressed);

        optionsPanel.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(OptionsReturnPressed);

        pausePanel.transform.SetParent(canvas.transform);
        pausePanel.transform.localPosition = Vector2.zero;
        optionsPanel.transform.SetParent(canvas.transform);
        optionsPanel.transform.localPosition = Vector2.zero;
        
        pausePanel.SetActive(false);
        optionsPanel.SetActive(false);
    }

    private void SetupBuildPanels() {
        pausePanel = (GameObject)Instantiate(Resources.Load("Pause/PauseMain-Build"));
        optionsPanel = (GameObject)Instantiate(Resources.Load("Pause/PauseOptions"));
        savePanel = (GameObject)Instantiate(Resources.Load("SavePanel"));

        pausePanel.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(ResumePressed);
        pausePanel.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(SavePressed);
        pausePanel.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(LoadPressed);
        pausePanel.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(OptionsPressed);
        pausePanel.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(QuitPressed);

        optionsPanel.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(OptionsReturnPressed);

        savePanel.transform.Find("SaveButton").GetComponent<Button>().onClick.AddListener(Save_SavePressed);
        savePanel.transform.Find("CancelButton").GetComponent<Button>().onClick.AddListener(Save_CancelPressed);
        savePanel.transform.Find("NameInput").GetComponent<InputField>().onValueChanged.AddListener(Save_NameInputChanged);

        pausePanel.transform.SetParent(canvas.transform);
        pausePanel.transform.localPosition = Vector2.zero;
        optionsPanel.transform.SetParent(canvas.transform);
        optionsPanel.transform.localPosition = Vector2.zero;
        savePanel.transform.SetParent(canvas.transform);
        savePanel.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        savePanel.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        pausePanel.SetActive(false);
        optionsPanel.SetActive(false);
        savePanel.SetActive(false);
    }

    public static string SanitizeFileName(string fileName) {
        foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            fileName = fileName.Replace(c, '_');
        if (fileName.Length > 32)
            fileName = fileName.Substring(0, 32);
        return fileName;
    }

    private void SetupBackgroundFade() {
        bgFade = new GameObject("PauseBGFade", typeof(Image));
        bgFade.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0.5f);
        bgFade.transform.SetParent(canvas.transform);
        //bgFade.transform.SetAsFirstSibling();
        bgFade.GetComponent<RectTransform>().anchorMin = Vector2.zero;
        bgFade.GetComponent<RectTransform>().anchorMax = Vector2.one;
        bgFade.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        bgFade.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        bgFade.SetActive(false);
    }
}
