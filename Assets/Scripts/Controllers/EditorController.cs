﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditorController : MonoBehaviour {

    // Attach me to the main camera! :D

    public GameObject shapeTypePanel, blockPanel;

    public KeyCode moveUpKey;
    public KeyCode moveDownKey;
    public KeyCode moveLeftKey;
    public KeyCode moveRightKey;
    public KeyCode zoomIn;
    public KeyCode zoomOut;
    public KeyCode boostKey;

    private Color selectionGlowNormal = new Color(1f, 0.9f, 0f);
    private Color selectionGlowMoving = new Color(1f, 0.6f, 0f);
    private Color selectionGlowColor;

    private Material sharedSelectionMaterial;

    private const float MAX_SPEED_ZOOMIN = 2.0F;
    private const float MAX_SPEED_ZOOMOUT = 5.0F;
    private const float SPEED_BOOST_MULTIPLIER = 1.5F;
    private const float MIN_ZOOM = 60f;
    private const float MAX_ZOOM = 300f;
    private const float ZOOM_STEP = 20f;

    private bool scrollUp, scrollDown, boost, up, down, left, right, moveUDKU, moveUDKD, moveLRKU, moveLRKD;

    private float zoom;
    private float moveTimeX, moveTimeY, zoomTime;

    private Vector2 blockPos;
    private Vector2 velocity, currentVelocity;
    private Vector2 halfScreen;

    public bool isStartPosChosen;
    public Vector2 startPosBlockAligned;

    public RadioButtonTools buttons;

    public float highlightFloat;

    public bool paused;

    bool savePosAvailable;
    GameObject ghost, savePosFill;
    ushort selectedBlockID, selectedEntityID;

    Vector2 startDragPos, newDragPos, mousePosLast;
    bool drawing, erasing, mouseMoved;
    List<GameObject> drawnBlocks, removedBlocks, erasingBlocks;
    bool shapeFill;
    SelectTool selectTool;
    PencilTool pencilTool;
    ShapeTool shapeTool;
    EntityTool entityTool;
    ItemTool itemTool;
    PlayerTool playerTool;
    Tool selectedTool;
    public bool shapeToolSelected;
    Tool lastDrawingToolSelected;
    bool ctrlSelectEnabled;

    List<GameObject> selectedBlocks, copiedBlocks;
    private bool movingBlocks, copyingBlocks;
    Vector2 movingOffset;

    Stack<UndoItem> undoHistory, redoHistory;

    void Start() {
        buttons = new RadioButtonTools();
        buttons.SetActive(RadioTool.PENCIL);
        selectTool = new SelectTool();
        pencilTool = new PencilTool();
        shapeTool = new ShapeTool();
        entityTool = new EntityTool();
        itemTool = new ItemTool();
        playerTool = new PlayerTool();
        selectedTool = pencilTool;
        lastDrawingToolSelected = pencilTool;
        selectedBlocks = new List<GameObject>();
        copiedBlocks = new List<GameObject>();
        erasingBlocks = new List<GameObject>();
        undoHistory = new Stack<UndoItem>();
        redoHistory = new Stack<UndoItem>();
        zoom = GetComponent<Camera>().orthographicSize;

        float halfScreenX = GetComponent<Camera>().orthographicSize * Screen.width / Screen.height;
        float halfScreenY = GetComponent<Camera>().orthographicSize;
        halfScreen = new Vector2(halfScreenX, halfScreenY);
        transform.position = new Vector3(halfScreen.x, halfScreen.y, transform.position.z);

        selectedBlockID = 1;
        drawnBlocks = new List<GameObject>();
        removedBlocks = new List<GameObject>();
        ghost = new GameObject("Ghost", typeof(SpriteRenderer));
        ghost.layer = LayerMask.NameToLayer("UI");
        ghost.transform.localScale = new Vector3(4f, 4f, ghost.transform.position.z);
        ghost.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(selectedBlockID), "ghost", -8, -8);
        ghost.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Foreground");
        Color color = ghost.GetComponent<SpriteRenderer>().color;
        color.a = 0.4f;
        ghost.GetComponent<SpriteRenderer>().color = color;

        selectionGlowColor = selectionGlowNormal;
        sharedSelectionMaterial = (Material)Resources.Load("GlowMaterial");
        sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
        sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
    }

    void Update() {
        if (paused) {
            foreach (GameObject block in drawnBlocks)
                Destroy(block);
            drawnBlocks.Clear();
            foreach (GameObject block in erasingBlocks)
                block.SetActive(true);
            erasingBlocks.Clear();
            if (selectedTool == shapeTool && drawing && shapeFill) {
                if (GameObject.Find("FakeFill") != null) {
                    Destroy(GameObject.Find("FakeFill"));
                }
            }
            drawing = false;
            erasing = false;

            return;
        }

        UpdateInputFlags();

        shapeToolSelected = selectedTool == shapeTool;

        // No one should reach level max feasibly, but... put this hard stopper in anyway, because why not
        if (transform.position.x > 65535 - halfScreen.x)
            transform.position = new Vector3(65535 - halfScreen.x, transform.position.y, transform.position.z);
        if (transform.position.y > 65535 - halfScreen.y)
            transform.position = new Vector3(transform.position.x, 65535 - halfScreen.y, transform.position.z);

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        mouseMoved = floorVector2ToX(mousePosLast, 16) != floorVector2ToX(mousePos, 16);

        blockPos = new Vector2(PencilTool.floorToX(mousePos.x, 16), PencilTool.floorToX(mousePos.y, 16));

        ghost.transform.position = new Vector3(blockPos.x, blockPos.y, transform.position.z);

        if (selectedTool == playerTool) {
            if (savePosFill == null) {
                savePosFill = GameObject.CreatePrimitive(PrimitiveType.Plane);
                savePosFill.name = "savePosFillHighlight";
                savePosFill.GetComponent<MeshCollider>().enabled = false;
                savePosFill.GetComponent<MeshRenderer>().receiveShadows = false;
                savePosFill.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                savePosFill.GetComponent<MeshRenderer>().sortingLayerName = "Foreground";
                savePosFill.GetComponent<MeshRenderer>().sortingOrder = 1;
                savePosFill.transform.localRotation = Quaternion.Euler(90, 0, 180);
                savePosFill.GetComponent<MeshRenderer>().material = (Material)Resources.Load("UnlitSolidColor");
                savePosFill.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, highlightFloat);
                savePosFill.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", new Color(0.2f, 0.8f, 0.2f));
            } else if (savePosFill.activeSelf == false) {
                savePosFill.SetActive(true);
            }
            savePosFill.transform.localPosition = new Vector3(blockPos.x + 8, blockPos.y + 16, savePosFill.transform.localPosition.z);
            savePosFill.transform.localScale = new Vector3(1.6f, 1, 3.2f);
        } else {
            if (savePosFill != null && isStartPosChosen == false && savePosFill.activeSelf == true)
                savePosFill.SetActive(false);
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) {

            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            pointerData.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            startDragPos = mousePos;
            mousePosLast = mousePos;
            if (Input.GetMouseButtonDown(0)) {
                drawing = true;
            } else if (Input.GetMouseButtonDown(1)) {
                erasing = true;
            }

            foreach (RaycastResult r in results) {
                if (r.gameObject.name != "Canvas") {
                    drawing = false;
                    erasing = false;
                    break;
                } else {
                    if ((Input.GetMouseButton(0) && Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0) && Input.GetMouseButton(1)) && movingBlocks) {
                        movingBlocks = false;
                        selectionGlowColor = selectionGlowNormal;
                        sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
                        sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
                        foreach (GameObject block in selectedBlocks) {
                            block.GetComponent<SpriteRenderer>().sortingOrder = 0;
                            block.transform.localPosition = new Vector3(block.transform.localPosition.x - movingOffset.x,
                        block.transform.localPosition.y - movingOffset.y, block.transform.localPosition.z);
                        }
                    } else {
                        Collider2D[] collisions = Physics2D.OverlapPointAll(mousePos);
                        foreach (Collider2D col in collisions) {
                            if (selectedBlocks.Contains(col.gameObject)) {
                                movingBlocks = true;
                                drawing = false;
                                erasing = false;
                                movingOffset = Vector2.zero;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1)) {
            if (movingBlocks && mouseMoved) {
                selectionGlowColor = selectionGlowMoving;
                sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
                sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
                mouseMoved = false;
                newDragPos = mousePos;
                movingOffset.Set(movingOffset.x + (floorToX(mousePos.x, 16) - floorToX(mousePosLast.x, 16)),
                    movingOffset.y + (floorToX(mousePos.y, 16) - floorToX(mousePosLast.y, 16)));
                foreach (GameObject block in selectedBlocks) {
                    block.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    block.transform.localPosition = new Vector3(block.transform.localPosition.x + (floorToX(mousePos.x, 16) - floorToX(mousePosLast.x, 16)),
                        block.transform.localPosition.y + (floorToX(mousePos.y, 16) - floorToX(mousePosLast.y, 16)), block.transform.localPosition.z);
                }
                mousePosLast = mousePos;
            } else if ((drawing || erasing) && mouseMoved) {
                selectionGlowColor = selectionGlowNormal;
                sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
                sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
                mousePosLast = mousePos;
                mouseMoved = false;
                newDragPos = mousePos;

                if (selectedTool == selectTool) {
                    selectedTool.Draw(selectedBlockID, startDragPos, newDragPos, drawnBlocks, removedBlocks, (shapeFill ? 1 : 0));
                } else {
                    // Ugh, this bit CAN'T be good from a memory management perspective. (EVERY FRAME TOO, YIKES)
                    // There's gotta be a better way to traverse a data structure while deleting each element?
                    foreach (GameObject block in drawnBlocks) {
                        Destroy(block);
                    }
                    drawnBlocks = new List<GameObject>();
                    removedBlocks = new List<GameObject>();
                    if (Input.GetMouseButton(1)) {
                        foreach(GameObject go in erasingBlocks) {
                            go.SetActive(true);
                        }
                        erasingBlocks = new List<GameObject>();
                        selectedTool.Draw(selectedBlockID, startDragPos, newDragPos, drawnBlocks, removedBlocks, (shapeFill ? 1 : 0));
                        foreach(GameObject go in removedBlocks) {
                            go.SetActive(false);
                            erasingBlocks.Add(go);
                        }
                    } else {
                        selectedTool.Draw(selectedBlockID, startDragPos, newDragPos, drawnBlocks, removedBlocks, (shapeFill ? 1 : 0));
                    }
                }
            }
        }
        if (Input.GetMouseButtonDown(1) && drawing || Input.GetMouseButtonDown(0) && erasing) {
            drawing = false;
            erasing = false;
            foreach (GameObject block in drawnBlocks) {
                Destroy(block);
            }

            // Shape tool uses a fake filled square for its draw function - remove fake fill when prematurely stopping draw
            GameObject fakeFill = GameObject.Find("FakeFill");
            if (fakeFill != null)
                Destroy(fakeFill);
            GameObject s = GameObject.Find("SelectionHighlight");
            if (s != null)
                Destroy(s);

            drawnBlocks = new List<GameObject>();
            erasingBlocks = new List<GameObject>();

        }
        if ((Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)) && (movingBlocks || copyingBlocks)) {
            selectionGlowColor = selectionGlowNormal;
            sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
            sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
            if (copyingBlocks) {
                copyingBlocks = false;
                movingBlocks = false;
                Dictionary<Vector2, int> added = new Dictionary<Vector2, int>();
                Dictionary<Vector2, int> removed = new Dictionary<Vector2, int>();
                foreach (GameObject selected in selectedBlocks) {
                    added.Add(selected.transform.position, selected.GetComponent<Block>().blockID);
                    selected.GetComponent<BoxCollider2D>().enabled = false;

                    Collider2D old = Physics2D.OverlapPoint(new Vector2(selected.transform.localPosition.x + 8, selected.transform.localPosition.y + 8));
                    if (old != null) {
                        removed.Add(old.gameObject.transform.position, old.gameObject.GetComponent<Block>().blockID);
                        Destroy(old.gameObject);
                    }

                    selected.GetComponent<SpriteRenderer>().sortingOrder = 0;
                    selected.GetComponent<BoxCollider2D>().enabled = true;
                }
                undoHistory.Push(new UndoItem(UndoType.BOTH, added, removed));
                redoHistory = new Stack<UndoItem>();
            } else {
                movingBlocks = false;
                List<Vector2> moved = new List<Vector2>();
                Dictionary<Vector2, int> removed = new Dictionary<Vector2, int>();
                foreach (GameObject selected in selectedBlocks) {
                    //selected.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    selected.GetComponent<BoxCollider2D>().enabled = false;

                    moved.Add(selected.transform.localPosition);
                    Collider2D old = Physics2D.OverlapPoint(new Vector2(selected.transform.localPosition.x + 8, selected.transform.localPosition.y + 8));
                    if (old != null) {
                        removed.Add(old.gameObject.transform.position, old.gameObject.GetComponent<Block>().blockID);
                        Destroy(old.gameObject);
                    }

                    selected.GetComponent<SpriteRenderer>().sortingOrder = 0;
                    selected.GetComponent<BoxCollider2D>().enabled = true;
                }
                undoHistory.Push(new UndoItem(UndoType.MOVE, moved, movingOffset, removed));
                redoHistory = new Stack<UndoItem>();
            }
        }
        if (Input.GetMouseButtonUp(0) && !erasing || Input.GetMouseButtonUp(1) && !drawing) {
            if (drawing || erasing) {
                drawing = false;
                erasing = false;
                newDragPos = mousePos;

                foreach (GameObject block in drawnBlocks) {
                    Destroy(block);
                }

                if (selectedTool == selectTool) {
                    GameObject s = GameObject.Find("SelectionHighlight");
                    if (s != null)
                        Destroy(s);

                    if (!Input.GetKey(KeyCode.LeftShift)) {
                        DeselectSelectedGameObjects();
                    }

                    for (float y = selectTool.selectionStart.y; y <= selectTool.selectionEnd.y; y += 16) {
                        for (float x = selectTool.selectionStart.x; x <= selectTool.selectionEnd.x; x += 16) {
                            Collider2D[] col = Physics2D.OverlapPointAll(new Vector2(x + 8, y + 8));
                            foreach (Collider2D c in col) {
                                if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                                    if (c.GetComponent<SpriteRenderer>().sharedMaterials.Length == 1) {
                                        Material[] mats = new Material[2];
                                        mats[0] = c.GetComponent<SpriteRenderer>().sharedMaterial;
                                        mats[1] = sharedSelectionMaterial;
                                        c.GetComponent<SpriteRenderer>().sharedMaterials = mats;
                                    }
                                    if (!selectedBlocks.Contains(c.gameObject))
                                        selectedBlocks.Add(c.gameObject);
                                    //if (c.gameObject.GetComponent<BlockSelectedGlow>() == null) {
                                    //    c.gameObject.AddComponent<BlockSelectedGlow>();
                                    //    selectedBlocks.Add(c.gameObject);
                                    //} else {
                                    //    if (!c.gameObject.GetComponent<BlockSelectedGlow>().isActiveAndEnabled) { // bool false when destroyed
                                    //        c.gameObject.AddComponent<BlockSelectedGlow>();
                                    //        selectedBlocks.Add(c.gameObject);
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                } else {
                    drawnBlocks = new List<GameObject>();
                    removedBlocks = new List<GameObject>();
                    foreach (GameObject go in erasingBlocks) {
                        go.SetActive(true);
                    }
                    erasingBlocks = new List<GameObject>();
                    selectedTool.Draw(selectedBlockID, startDragPos, newDragPos, drawnBlocks, removedBlocks, (shapeFill ? 1 : 0));
                    if (drawnBlocks.Count > 0 && removedBlocks.Count == 0) {
                        Dictionary<Vector2, int> added = new Dictionary<Vector2, int>();
                        foreach (GameObject b in drawnBlocks) {
                            added.Add(b.transform.position, b.GetComponent<Block>().blockID);
                        }
                        undoHistory.Push(new UndoItem(UndoType.ADD, added));
                        redoHistory = new Stack<UndoItem>();
                        drawnBlocks = new List<GameObject>();
                        removedBlocks = new List<GameObject>();
                    } else if (removedBlocks.Count > 0 && drawnBlocks.Count == 0) {
                        Dictionary<Vector2, int> removed = new Dictionary<Vector2, int>();
                        foreach (GameObject b in removedBlocks) {
                            removed.Add(b.transform.position, b.GetComponent<Block>().blockID);
                            Destroy(b);
                        }
                        undoHistory.Push(new UndoItem(UndoType.REMOVE, removed));
                        redoHistory = new Stack<UndoItem>();
                        drawnBlocks = new List<GameObject>();
                        removedBlocks = new List<GameObject>();
                    } else if (drawnBlocks.Count > 0 && removedBlocks.Count > 0) {
                        Dictionary<Vector2, int> added = new Dictionary<Vector2, int>();
                        Dictionary<Vector2, int> removed = new Dictionary<Vector2, int>();
                        foreach (GameObject b in drawnBlocks) {
                            added.Add(b.transform.position, b.GetComponent<Block>().blockID);
                        }
                        foreach (GameObject b in removedBlocks) {
                            removed.Add(b.transform.position, b.GetComponent<Block>().blockID);
                            Destroy(b);
                        }
                        undoHistory.Push(new UndoItem(UndoType.BOTH, added, removed));
                        redoHistory = new Stack<UndoItem>();
                        drawnBlocks = new List<GameObject>();
                        removedBlocks = new List<GameObject>();
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && selectedTool == playerTool && savePosAvailable) {
            isStartPosChosen = true;
            startPosBlockAligned = blockPos;
            Camera.main.GetComponent<GameMenuManager>().ShowSaveMenu();
        }
        if (Input.GetKeyDown(KeyCode.LeftControl) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks && selectedTool != playerTool) {
            ctrlSelectEnabled = true;
            lastDrawingToolSelected = selectedTool;
            SelectPressed();
        }
        if (Input.GetKeyUp(KeyCode.LeftControl) && ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks && selectedTool != playerTool) {
            ctrlSelectEnabled = false;
            ResetToolToLastUsed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks) {
            SelectPressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks) {
            PencilPressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks) {
            ShapePressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks) {
            EntityPressed();
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) && !ctrlSelectEnabled && !drawing && !erasing && !movingBlocks && !copyingBlocks) {
            ItemPressed();
        }
        if ((Application.isEditor ? true : Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.Z) && !drawing && selectedTool != playerTool) {
            DoUndoCommand();
        }
        if ((Application.isEditor ? true : Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.Y) && !drawing && selectedTool != playerTool) {
            DoRedoCommand();
        }
        if ((Application.isEditor ? true : Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.C) && !drawing && selectedTool != playerTool) {
            DoCopyCommand();
        }
        if ((Application.isEditor ? true : Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.X) && !drawing && selectedTool != playerTool) {
            DoCutCommand();
        }
        if ((Application.isEditor ? true : Input.GetKey(KeyCode.LeftControl)) && Input.GetKeyDown(KeyCode.V) && !drawing && selectedTool != playerTool) {
            DoPasteCommand();
        }
        if (Input.GetKeyDown(KeyCode.Delete)) {
            DoDeleteCommand();
        }
    }
    
    private void UpdateInputFlags() {
        if (Input.GetKeyDown(zoomIn) || Input.GetAxis("Mouse ScrollWheel") > 0)
            scrollUp = true;
        if (Input.GetKeyDown(zoomOut) || Input.GetAxis("Mouse ScrollWheel") < 0)
            scrollDown = true;
        if (Input.GetKey(boostKey) && !Input.GetMouseButton(0))
            boost = true;
        if (Input.GetKey(moveUpKey))
            up = true;
        if (Input.GetKey(moveDownKey))
            down = true;
        if (Input.GetKey(moveLeftKey))
            left = true;
        if (Input.GetKey(moveRightKey))
            right = true;
        if (Input.GetKeyUp(moveUpKey) || Input.GetKeyUp(moveDownKey))
            moveUDKU = true;
        if (Input.GetKeyDown(moveUpKey) || Input.GetKeyDown(moveDownKey))
            moveUDKD = true;
        if (Input.GetKeyUp(moveLeftKey) || Input.GetKeyUp(moveRightKey))
            moveLRKU = true;
        if (Input.GetKeyDown(moveLeftKey) || Input.GetKeyDown(moveRightKey))
            moveLRKD = true;

        ShapeOptionsMenu shapeOptionsMenu = shapeTypePanel.GetComponent<ShapeOptionsMenu>();
        if (Input.GetKeyDown(KeyCode.F) && selectedTool == shapeTool) {
            if (shapeOptionsMenu.selected == ShapeRadioButton.FILLED) {
                shapeTypePanel.transform.GetChild(0).GetComponent<Toggle>().isOn = true;
            } else {
                shapeTypePanel.transform.GetChild(1).GetComponent<Toggle>().isOn = true;
            }
        }
        shapeFill = shapeOptionsMenu.selected == ShapeRadioButton.FILLED;
    }


    private void DeselectSelectedGameObjects() {
        foreach (GameObject oldSelected in selectedBlocks) {
            Material[] mats = new Material[1];
            mats[0] = oldSelected.GetComponent<SpriteRenderer>().sharedMaterials[0];
            oldSelected.GetComponent<SpriteRenderer>().sharedMaterials = mats;
        }
        selectedBlocks = new List<GameObject>();
    }

    private void DoUndoCommand() {
        if (undoHistory.Count > 0) {
            DeselectSelectedGameObjects();
            UndoItem undo = undoHistory.Pop();
            redoHistory.Push(undo);
            if (undo.GetUndoType() == UndoType.ADD) {
                foreach (Vector2 pos in undo.GetAddedBlocks().Keys)
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                            Destroy(col.gameObject);
            } else if (undo.GetUndoType() == UndoType.REMOVE) {
                foreach (Vector2 pos in undo.GetRemovedBlocks().Keys) {
                    GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                    newBlock.transform.position = pos;
                    newBlock.GetComponent<Block>().SetupBlock((ushort)undo.GetRemovedBlocks()[pos]);
                    newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(undo.GetRemovedBlocks()[pos]), "ghost", -8, -8);
                    newBlock.transform.parent = GameObject.Find("Blocks").transform;
                }
            } else if (undo.GetUndoType() == UndoType.BOTH) {
                foreach (Vector2 pos in undo.GetAddedBlocks().Keys)
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                            Destroy(col.gameObject);

                foreach (Vector2 pos in undo.GetRemovedBlocks().Keys) {
                    GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                    newBlock.transform.position = pos;
                    newBlock.GetComponent<Block>().SetupBlock((ushort)undo.GetRemovedBlocks()[pos]);
                    newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(undo.GetRemovedBlocks()[pos]), "ghost", -8, -8);
                    newBlock.transform.parent = GameObject.Find("Blocks").transform;
                }
            } else if (undo.GetUndoType() == UndoType.MOVE) {
                List<GameObject> undoMoved = new List<GameObject>();
                foreach (Vector2 pos in undo.GetMovedBlocks()) {
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8))) {
                        if (undoMoved.Contains(col.gameObject))
                            continue;
                        col.gameObject.transform.localPosition = new Vector3(col.gameObject.transform.localPosition.x - undo.GetOffset().x,
                            col.gameObject.transform.localPosition.y - undo.GetOffset().y, col.gameObject.transform.localPosition.z);
                        undoMoved.Add(col.gameObject);
                    }
                }

                foreach (Vector2 pos in undo.GetRemovedBlocks().Keys) {
                    GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                    newBlock.transform.position = pos;
                    newBlock.GetComponent<Block>().SetupBlock((ushort)undo.GetRemovedBlocks()[pos]);
                    newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(undo.GetRemovedBlocks()[pos]), "ghost", -8, -8);
                    newBlock.transform.parent = GameObject.Find("Blocks").transform;
                }

                //foreach (Vector2 pos in redo.GetRemovedBlocks().Keys)
                //    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                //        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                //            Destroy(col.gameObject);

                //foreach (Vector2 pos in redo.GetMovedBlocks()) {
                //    Collider2D col = Physics2D.OverlapPoint(new Vector2(pos.x - redo.GetOffset().x + 8, pos.y - redo.GetOffset().y + 8));
                //    col.gameObject.transform.localPosition = new Vector3(col.gameObject.transform.localPosition.x + redo.GetOffset().x,
                //        col.gameObject.transform.localPosition.y + redo.GetOffset().y, col.gameObject.transform.localPosition.z);

                //}
            }
        }
    }

    private void DoRedoCommand() {
        if (redoHistory.Count > 0) {
            DeselectSelectedGameObjects();
            UndoItem redo = redoHistory.Pop();
            undoHistory.Push(redo);
            if (redo.GetUndoType() == UndoType.ADD) {
                foreach (Vector2 pos in redo.GetAddedBlocks().Keys) {
                    GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                    newBlock.transform.position = pos;
                    newBlock.GetComponent<Block>().SetupBlock((ushort)redo.GetAddedBlocks()[pos]);
                    newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(redo.GetAddedBlocks()[pos]), "ghost", -8, -8);
                    newBlock.transform.parent = GameObject.Find("Blocks").transform;
                }
            } else if (redo.GetUndoType() == UndoType.REMOVE) {
                foreach (Vector2 pos in redo.GetRemovedBlocks().Keys)
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                            Destroy(col.gameObject);
            } else if (redo.GetUndoType() == UndoType.BOTH) {
                foreach (Vector2 pos in redo.GetRemovedBlocks().Keys)
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                            Destroy(col.gameObject);

                foreach (Vector2 pos in redo.GetAddedBlocks().Keys) {
                    GameObject newBlock = GameObject.Instantiate(Resources.Load<GameObject>("Block"));
                    newBlock.transform.position = pos;
                    newBlock.GetComponent<Block>().SetupBlock((ushort)redo.GetAddedBlocks()[pos]);
                    newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(redo.GetAddedBlocks()[pos]), "ghost", -8, -8);
                    newBlock.transform.parent = GameObject.Find("Blocks").transform;
                }
            } else if (redo.GetUndoType() == UndoType.MOVE) {
                foreach (Vector2 pos in redo.GetRemovedBlocks().Keys) {
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x + 8, pos.y + 8)))
                        if (col.gameObject.layer == LayerMask.NameToLayer("Block"))
                            Destroy(col.gameObject);
                }

                List<GameObject> redoMoved = new List<GameObject>();
                foreach (Vector2 pos in redo.GetMovedBlocks()) {
                    foreach (Collider2D col in Physics2D.OverlapPointAll(new Vector2(pos.x - redo.GetOffset().x + 8, pos.y - redo.GetOffset().y + 8))) {
                        if (redoMoved.Contains(col.gameObject))
                            continue;
                        col.gameObject.transform.localPosition = new Vector3(col.gameObject.transform.localPosition.x + redo.GetOffset().x,
                            col.gameObject.transform.localPosition.y + redo.GetOffset().y, col.gameObject.transform.localPosition.z);
                        redoMoved.Add(col.gameObject);
                    }
                }
            }
        }
    }

    public void DoCopyCommand() {
        SelectPressed();
        foreach (GameObject old in copiedBlocks) {
            Destroy(old);
        }
        copiedBlocks = new List<GameObject>();

        foreach (GameObject block in selectedBlocks) {
            GameObject copy = Instantiate(block);
            copy.name = block.name;
            copy.transform.parent = block.transform.parent.transform;
            copy.SetActive(false);
            copiedBlocks.Add(copy);
        }
    }

    public void DoCutCommand() {
        SelectPressed();
        Dictionary<Vector2, int> deleted = new Dictionary<Vector2, int>();
        foreach (GameObject old in copiedBlocks) {
            Destroy(old);
        }
        copiedBlocks = new List<GameObject>();

        foreach (GameObject block in selectedBlocks) {
            deleted.Add(block.transform.position, block.GetComponent<Block>().blockID);
            block.SetActive(false);
            copiedBlocks.Add(block);
        }
        undoHistory.Push(new UndoItem(UndoType.REMOVE, deleted));
        redoHistory = new Stack<UndoItem>();
    }

    public void DoPasteCommand() {
        SelectPressed();
        if (copyingBlocks) {
            Dictionary<Vector2, int> addedBlocks = new Dictionary<Vector2, int>();
            Dictionary<Vector2, int> removedBlocks = new Dictionary<Vector2, int>();
            foreach (GameObject selected in selectedBlocks) {
                addedBlocks.Add(selected.transform.position, selected.GetComponent<Block>().blockID);
                selected.GetComponent<BoxCollider2D>().enabled = false;

                Collider2D old = Physics2D.OverlapPoint(new Vector2(selected.transform.localPosition.x + 8, selected.transform.localPosition.y + 8));
                if (old != null) {
                    removedBlocks.Add(old.gameObject.transform.position, old.gameObject.GetComponent<Block>().blockID);
                    Destroy(old.gameObject);
                }

                selected.GetComponent<SpriteRenderer>().sortingOrder = 0;
                selected.GetComponent<BoxCollider2D>().enabled = true;
            }

            undoHistory.Push(new UndoItem(UndoType.BOTH, addedBlocks, removedBlocks));
            redoHistory = new Stack<UndoItem>();
        }
        DeselectSelectedGameObjects();
        selectionGlowColor = selectionGlowMoving;
        sharedSelectionMaterial.SetColor("_EmissionColor", selectionGlowColor);
        sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
        Dictionary<Vector2, int> added = new Dictionary<Vector2, int>();
        foreach (GameObject block in copiedBlocks) {
            added.Add(block.transform.position, block.GetComponent<Block>().blockID);
            GameObject copy = Instantiate(block);
            copy.transform.parent = block.transform.parent.transform;
            copy.name = block.name;
            selectedBlocks.Add(copy);
            Material[] mats = new Material[2];
            mats[0] = copy.GetComponent<SpriteRenderer>().sharedMaterial;
            mats[1] = sharedSelectionMaterial;
            copy.GetComponent<SpriteRenderer>().sharedMaterials = mats;
            copy.GetComponent<SpriteRenderer>().sortingOrder = 1;
            copy.SetActive(true);
        }
        copyingBlocks = true;
        movingBlocks = false;
        drawing = false;
        erasing = false;
    }

    public void DoDeleteCommand() {
        Dictionary<Vector2, int> deleted = new Dictionary<Vector2, int>();
        foreach (GameObject block in selectedBlocks) {
            deleted.Add(block.transform.position, block.GetComponent<Block>().blockID);
            Destroy(block);
        }
        selectedBlocks.Clear();
        undoHistory.Push(new UndoItem(UndoType.REMOVE, deleted));
        redoHistory = new Stack<UndoItem>();
    }

    public void SetSelectedBlockID(ushort id) {
        if (!BlockID.isValidID(id)) {
            Debug.LogError("Editor: INVALID BLOCK ID " + id);
            return;
        }

        selectedBlockID = id;
        ghost.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(selectedBlockID), "ghost", -8, -8);

        blockPanel.GetComponent<BlockPanelManager>().setSelectedBlockID(id);

        ResetToolToLastUsed();
    }

    void FixedUpdate() {
        highlightFloat = (Mathf.Sin(Time.timeSinceLevelLoad * 3) / 5f) + 0.5f;
        sharedSelectionMaterial.color = new Color(selectionGlowColor.r, selectionGlowColor.g, selectionGlowColor.b, highlightFloat);
        GameObject selection = GameObject.Find("SelectionHighlight");
        if (selection != null)
            selection.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, highlightFloat);
        if (savePosFill != null) {
            Collider2D[] bottom, top;
            if (isStartPosChosen) {
                Vector2 chosen = startPosBlockAligned;
                bottom = Physics2D.OverlapPointAll(new Vector2(chosen.x + 8, chosen.y + 8));
                top = Physics2D.OverlapPointAll(new Vector2(chosen.x + 8, chosen.y + 24));
            } else {
                bottom = Physics2D.OverlapPointAll(new Vector2(blockPos.x + 8, blockPos.y + 8));
                top = Physics2D.OverlapPointAll(new Vector2(blockPos.x + 8, blockPos.y + 24));
            }
            savePosFill.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f, highlightFloat);
            
            savePosAvailable = true;
            foreach (Collider2D col in bottom) {
                if (col.gameObject.layer == LayerMask.NameToLayer("Block")) {
                    savePosAvailable = false;
                    break;
                }
            }
            if (savePosAvailable) {
                foreach (Collider2D col in top) {
                    if (col.gameObject.layer == LayerMask.NameToLayer("Block")) {
                        savePosAvailable = false;
                        break;
                    }
                }
            }
            if (savePosAvailable) {
                savePosFill.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", new Color(0.2f, 0.8f, 0.2f));
            } else {
                savePosFill.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", new Color(0.8f, 0.2f, 0.2f));
            }
            
        }
            
        //if (scrollUp && selectedBlockID < 18)
        //    SetSelectedBlockID((ushort)(selectedBlockID + 1));
        //if (scrollDown && selectedBlockID > 1)
        //    SetSelectedBlockID((ushort)(selectedBlockID - 1));
        ghost.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(selectedBlockID), "ghost", -8, -8);

        if (selectedTool == selectTool || selectedTool == playerTool) {
            if (ghost.GetComponent<SpriteRenderer>().color.a != 0f) {
                Color color = ghost.GetComponent<SpriteRenderer>().color;
                color.a = 0f;
                ghost.GetComponent<SpriteRenderer>().color = color;
            }
        } else {
            if (ghost.GetComponent<SpriteRenderer>().color.a != 0.4f) {
                Color color = ghost.GetComponent<SpriteRenderer>().color;
                color.a = 0.4f;
                ghost.GetComponent<SpriteRenderer>().color = color;
            }
        }
        moveTimeX += Time.deltaTime;
        moveTimeY += Time.deltaTime;
        zoomTime += Time.deltaTime;

        // Key up (slow down)
        if (moveUDKU)
            moveTimeY = Time.deltaTime * 8;
        if (moveLRKU)
            moveTimeX = Time.deltaTime * 8;

        // Key down (speed up in direction)
        if (moveUDKD)
            moveTimeY = Time.deltaTime * 8;
        else if (moveLRKD)
            moveTimeX = Time.deltaTime * 8;

        if (up && !down) {
            velocity = new Vector2(velocity.x, (MAX_SPEED_ZOOMIN + ( (MAX_SPEED_ZOOMOUT-MAX_SPEED_ZOOMIN) * ((zoom-MIN_ZOOM)/(MAX_ZOOM-MIN_ZOOM)) )) * (boost ? SPEED_BOOST_MULTIPLIER : 1));
        } else if (!up && down) {
            velocity = new Vector2(velocity.x, (-MAX_SPEED_ZOOMIN - ( (MAX_SPEED_ZOOMOUT - MAX_SPEED_ZOOMIN) * ((zoom - MIN_ZOOM) / (MAX_ZOOM - MIN_ZOOM)) )) * (boost ? SPEED_BOOST_MULTIPLIER : 1));
        } else {
            velocity = new Vector2(velocity.x, 0f);
        }
        if (left && !right) {
            velocity = new Vector2((-MAX_SPEED_ZOOMIN - ( (MAX_SPEED_ZOOMOUT - MAX_SPEED_ZOOMIN) * ((zoom - MIN_ZOOM) / (MAX_ZOOM - MIN_ZOOM))) ) * (boost ? SPEED_BOOST_MULTIPLIER : 1), velocity.y);
        } else if (!left && right) {
            velocity = new Vector2((MAX_SPEED_ZOOMIN + ( (MAX_SPEED_ZOOMOUT - MAX_SPEED_ZOOMIN) * ((zoom - MIN_ZOOM) / (MAX_ZOOM - MIN_ZOOM))) ) * (boost ? SPEED_BOOST_MULTIPLIER : 1), velocity.y);
        } else {
            velocity = new Vector2(0f, velocity.y);
        }
        
        if (scrollUp) {
            if (zoom <= MIN_ZOOM)
                zoom = MIN_ZOOM;
            else
                zoom -= ZOOM_STEP;
            zoomTime = Time.deltaTime * 10;
        }
        if (scrollDown) {
            if (zoom >= MAX_ZOOM)
                zoom = MAX_ZOOM;
            else
                zoom += ZOOM_STEP;
            zoomTime = Time.deltaTime * 10;
        }

        currentVelocity = new Vector2(Mathf.SmoothStep(currentVelocity.x, velocity.x, moveTimeX), Mathf.SmoothStep(currentVelocity.y, velocity.y, moveTimeY));
        transform.position = new Vector3(transform.position.x + currentVelocity.x, transform.position.y + currentVelocity.y, transform.position.z);

        GetComponent<Camera>().orthographicSize = Mathf.SmoothStep(GetComponent<Camera>().orthographicSize, zoom, zoomTime);

        // Reset input flags to false
        boost = false;
        scrollUp = false; scrollDown = false;
        up = false; down = false;
        left = false; right = false;
        moveUDKU = false; moveUDKD = false;
        moveLRKU = false; moveLRKD = false;

        float halfScreenX = GetComponent<Camera>().orthographicSize * Screen.width / Screen.height;
        float halfScreenY = GetComponent<Camera>().orthographicSize;
        halfScreen = new Vector2(halfScreenX, halfScreenY);

        if (transform.position.x < -halfScreen.x + 128)
            transform.position = new Vector3(-halfScreen.x + 128, transform.position.y, transform.position.z);
        if (transform.position.y < -halfScreen.y + 128)
            transform.position = new Vector3(transform.position.x, -halfScreen.y + 128, transform.position.z);
    }

    public void ResetToolToLastUsed() {
        if (lastDrawingToolSelected == pencilTool) {
            PencilPressed();
        }
        if (lastDrawingToolSelected == shapeTool) {
            ShapePressed();
        }
    }

    public void ResetSaveSpawn() {
        isStartPosChosen = false;
        startPosBlockAligned = Vector2.zero;
        savePosFill.SetActive(false);
        ResetToolToLastUsed();
    }

    public void pickPlayerPosAndSave() {
        buttons.SetActive(RadioTool.NONE);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = playerTool;
        savePosAvailable = false;
        DeselectSelectedGameObjects();
    }

    public void SelectPressed() {
        buttons.SetActive(RadioTool.SELECT);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = selectTool;
    }

    public void PencilPressed() {
        buttons.SetActive(RadioTool.PENCIL);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = pencilTool;
        lastDrawingToolSelected = pencilTool;

        DeselectSelectedGameObjects();
    }

    public void ShapePressed() {
        buttons.SetActive(RadioTool.SHAPE);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = shapeTool;
        lastDrawingToolSelected = shapeTool;

        DeselectSelectedGameObjects();
    }

    public void EntityPressed() {
        buttons.SetActive(RadioTool.ENTITY);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = entityTool;

        DeselectSelectedGameObjects();
    }

    public void ItemPressed() {
        buttons.SetActive(RadioTool.ITEM);
        GameObject.Find("UIManager").GetComponent<EditorUIManager>().updateSelectedTool(buttons.GetActive());
        selectedTool = itemTool;

        DeselectSelectedGameObjects();
    }

    public void MenuPressed() {
        GetComponent<GameMenuManager>().BuildMenuPressed();
        paused = true;
    }

    public Vector2 floorVector2ToX(Vector2 v, int x) {
        return new Vector2(Mathf.Floor(v.x / x) * x, Mathf.Floor(v.y / x) * x);
    }

    public float floorToX(float n, float x) {
        return Mathf.Floor(n / x) * x;
    }
}
