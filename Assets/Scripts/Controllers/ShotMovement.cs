﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using System.Collections.Generic;

public class ShotMovement : MonoBehaviour {

    private const int SPEED = 4;
    private bool movingRight;

	void Start () {
        Destroy(gameObject, 1);
	}
	
    public void ChangeDirection() {
        movingRight = !movingRight;
    }

	// Once per frame (General per-frame code)
	void Update () {
	    if (movingRight) {
            transform.position = new Vector3(transform.position.x - SPEED, transform.position.y, transform.position.z);
        } else {
            transform.position = new Vector3(transform.position.x + SPEED, transform.position.y, transform.position.z);
        }
	}
}
