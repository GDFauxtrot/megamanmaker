﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour {

    public Vector2 worldMax;
    private GameObject mainPlayer;
    private Vector2 halfScreen;

	void Start () {
        foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Block")) {
            if (obj.transform.position.x > worldMax.x) {
                worldMax.x = obj.transform.position.x;
            }
            if (obj.transform.position.y > worldMax.y) {
                worldMax.y = obj.transform.position.y;
            }
        }
        worldMax = new Vector2(worldMax.x + 16, worldMax.y + 16);
        
        float halfScreenX = GetComponent<Camera>().orthographicSize * Screen.width / Screen.height;
        float halfScreenY = GetComponent<Camera>().orthographicSize;
        halfScreen = new Vector2(halfScreenX, halfScreenY);
        transform.position = new Vector3(halfScreenX, halfScreenY, transform.position.z);
        mainPlayer = GameObject.FindGameObjectWithTag("Player");
	}
	
    public void UpdateWorldMax(float xx, float yy) {
        worldMax = new Vector2((xx-1)*16, (yy-1)*16);
    }

	void Update () {
        float halfScreenX = GetComponent<Camera>().orthographicSize * Screen.width / Screen.height;
        float halfScreenY = GetComponent<Camera>().orthographicSize;
        halfScreen = new Vector2(halfScreenX, halfScreenY);
        if (mainPlayer.transform.position.x < worldMax.x - halfScreen.x) {
            transform.position = new Vector3(mainPlayer.transform.position.x, transform.position.y, transform.position.z);
        } else {
            transform.position = new Vector3(worldMax.x - halfScreen.x, transform.position.y, transform.position.z);
        }
        if (mainPlayer.transform.position.y < worldMax.y - halfScreen.y) {
            transform.position = new Vector3(transform.position.x, mainPlayer.transform.position.y, transform.position.z);
        } else {
            transform.position = new Vector3(transform.position.x, worldMax.y - halfScreen.y, transform.position.z);
        }
        if (transform.position.y < halfScreen.y) {
            transform.position = new Vector3(transform.position.x, halfScreen.y, transform.position.z);
        }
        if (transform.position.x < halfScreen.x) {
            transform.position = new Vector3(halfScreen.x, transform.position.y, transform.position.z);
        }
    }
}
