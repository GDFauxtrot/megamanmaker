﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditorUIManager : MonoBehaviour {

    GameObject menuButton, selectButton, pencilButton, shapeButton, entityButton, itemButton;
    Color pressed = new Color(0.8f, 0.8f, 0.8f);

    void Start() {
        menuButton = GameObject.Find("MenuButton");
        selectButton = GameObject.Find("SelectButton");
        pencilButton = GameObject.Find("PencilButton");
        shapeButton = GameObject.Find("ShapeButton");
        entityButton = GameObject.Find("EntityButton");
        itemButton = GameObject.Find("ItemButton");

        pencilButton.GetComponent<Button>().image.color = pressed;
    }

    public void updateSelectedTool(RadioTool tool) {
        selectButton.GetComponent<Button>().image.color = (tool == RadioTool.SELECT ? pressed : Color.white);
        pencilButton.GetComponent<Button>().image.color = (tool == RadioTool.PENCIL ? pressed : Color.white);
        shapeButton.GetComponent<Button>().image.color = (tool == RadioTool.SHAPE ? pressed : Color.white);
        entityButton.GetComponent<Button>().image.color = (tool == RadioTool.ENTITY ? pressed : Color.white);
        itemButton.GetComponent<Button>().image.color = (tool == RadioTool.ITEM ? pressed : Color.white);
    }
}
