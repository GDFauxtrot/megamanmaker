﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerAnimator : MonoBehaviour {

    private Dictionary<string, Sprite> sprites;
    private SpriteRenderer sprRender;
    private PlayerController player;
	private BoxCollider2D col2D;

    private int animFrame;

    void Start () {
        sprites = new Dictionary<string, Sprite>();
        sprRender = GetComponentInChildren<SpriteRenderer>();
        player = GetComponent<PlayerController>();
		col2D = GetComponent<BoxCollider2D>();

        // Importing sprites from Sprites/Resources folder
        sprites.Add("idle0", Texture2DImporter.LoadTexture2DToSprite("Player/mm_idle0", "idle0", 0, 1));
        sprites.Add("idle1", Texture2DImporter.LoadTexture2DToSprite("Player/mm_idle1", "idle1", 0, 1));
        sprites.Add("step", Texture2DImporter.LoadTexture2DToSprite("Player/mm_step", "step", 0, 1));
        sprites.Add("run0", Texture2DImporter.LoadTexture2DToSprite("Player/mm_run0", "run0", 0, 1));
        sprites.Add("run1", Texture2DImporter.LoadTexture2DToSprite("Player/mm_run1", "run1", 0, 1));
        sprites.Add("run2", Texture2DImporter.LoadTexture2DToSprite("Player/mm_run2", "run2", 0, 1));
        sprites.Add("jump", Texture2DImporter.LoadTexture2DToSprite("Player/mm_jump", "jump", 0, 1));
        sprites.Add("shot", Texture2DImporter.LoadTexture2DToSprite("FX/shot", "shot", 0, 1));
		sprites.Add("slide", Texture2DImporter.LoadTexture2DToSprite("Player/mm_slide", "slide", 0, 1));
        sprites.Add("shoot_stand", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_stand", "shoot_stand", 0, 1));
        sprites.Add("shoot_run0", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_run0", "shoot_run0", 0, 1));
        sprites.Add("shoot_run1", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_run1", "shoot_run1", 0, 1));
        sprites.Add("shoot_run2", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_run2", "shoot_run2", 0, 1));
        sprites.Add("shoot_jump", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_jump", "shoot_jump", 0, 1));
        sprites.Add("shoot_climb", Texture2DImporter.LoadTexture2DToSprite("Player/mm_shoot_climb", "shoot_climb", 0, 3));
        sprites.Add("climb", Texture2DImporter.LoadTexture2DToSprite("Player/mm_climb", "climb", 0, 3));
        sprites.Add("climbup", Texture2DImporter.LoadTexture2DToSprite("Player/mm_climbup", "climbup", 0, -3));
    }
	
	// Once per frame (General per-frame code)
	void Update () {
        sprRender.flipX = player.isFacingRight;
	}

	// Once per fixed interval (Physics, Animation)
	void FixedUpdate () {
        switch (player.GetState()) {
            case State.IDLE:
                // 99 frame loop
                // 90 frames: idle0
                // 9 frames: idle1
                if (animFrame >= 99)
                    animFrame = 0;

                if (animFrame < 90) {
                    sprRender.sprite = sprites["idle0"];
                } else if (animFrame < 99) {
                    sprRender.sprite = sprites["idle1"];
                }
                animFrame++;
                break;
            case State.RUN_START:
                // 9 frame sequence
                // 4 frames: idle0
                // 5 frames: step
                if (animFrame < 4) {
                    sprRender.sprite = sprites["idle0"];
                } else if (animFrame < 9) {
                    sprRender.sprite = sprites["step"];
                }
                animFrame++;
                break;
            case State.RUN:
                // 28 frame loop
                // 7 frames: run0
                // 7 frames: run1
                // 7 frames: run2
                // 7 frames: run1
                if (animFrame >= 28)
                    animFrame = 0;

                if (animFrame < 7) {
                    sprRender.sprite = sprites["run0"];
                } else if (animFrame < 14) {
                    sprRender.sprite = sprites["run1"];
                } else if (animFrame < 21) {
                    sprRender.sprite = sprites["run2"];
                } else if (animFrame < 28) {
                    sprRender.sprite = sprites["run1"];
                }
                animFrame++;
                break;
            case State.RUN_END:
                // 13 frame sequence
                // 6 frames: run1
                // 7 frames: step
                if (animFrame < 6) {
                    sprRender.sprite = sprites["run1"];
                } else if (animFrame < 13) {
                    sprRender.sprite = sprites["step"];
                }
                animFrame++;
                break;
            case State.MIDAIR:
                // No sequence
                sprRender.sprite = sprites["jump"];
                break;
            case State.LAND:
                // 3 frame sequence
                // 1 frame: run2
                // 2 frames: run0
                if (animFrame < 1) {
                    sprRender.sprite = sprites["run2"];
                } else if (animFrame < 3) {
                    sprRender.sprite = sprites["run0"];
                }
                animFrame++;
                break;
		    case State.SLIDE:
			    // No sequence
			    sprRender.sprite = sprites["slide"];
			    break;
		    case State.CLIMB:
                sprRender.sprite = sprites["climb"];
                if (player.animateClimb()) {
                    if (animFrame >= 9) {
                        player.isFacingRight = !player.isFacingRight;
                        animFrame = 0;
                    }
                    if (player.climbingUpTopLadder())
                        sprRender.sprite = sprites["climbup"];
                    if (!player.IsShooting())
                        animFrame++;
                } else {
                    animFrame = 0;
                }
			    break;
		    case State.HURT:
                break;
        }
        
		if (player.GetState() == State.SLIDE) {
			col2D.size = new Vector2(16f, 16f);
			col2D.offset = new Vector2(2f, -4f);
		} else {
			col2D.size = new Vector2(16f, 24f);
			col2D.offset = Vector2.zero;
		}
        SetSpriteShooting();
    }

    private void SetSpriteShooting() {
        if (player.IsShooting()) {
            switch (sprRender.sprite.name) {
                case "idle0":
                    sprRender.sprite = sprites["shoot_stand"];
                    break;
                case "idle1":
                    sprRender.sprite = sprites["shoot_stand"];
                    break;
                case "step":
                    sprRender.sprite = sprites["shoot_stand"];
                    break;
                case "run0":
                    sprRender.sprite = sprites["shoot_run0"];
                    break;
                case "run1":
                    sprRender.sprite = sprites["shoot_run1"];
                    break;
                case "run2":
                    sprRender.sprite = sprites["shoot_run2"];
                    break;
                case "jump":
                    sprRender.sprite = sprites["shoot_jump"];
                    break;
                case "climb":
                    sprRender.sprite = sprites["shoot_climb"];
                    break;
                case "climbup":
                    sprRender.sprite = sprites["shoot_climb"];
                    break;
            }
        }
    }

    /*
	public static Sprite LoadTexture2DToSprite(string texname, string sprname, bool pivotMoveDown = true) {
        Texture2D t2d = (Texture2D)Resources.Load(texname);
        Rect size = new Rect(0, 0, t2d.width, t2d.height);
		Vector2 pivot = new Vector2(0.5f, 0.5f);
		if (pivotMoveDown)
			pivot = new Vector2(0.5f, 0.5f + 1f/t2d.height);
		Sprite sprite = Sprite.Create(t2d, size, pivot, 4);
        sprite.name = sprname;
        return sprite;
    }
    */
    public void ResetAnimationTimer() {
        animFrame = 0;
    }
}
