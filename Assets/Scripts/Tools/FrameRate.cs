﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class FrameRate : MonoBehaviour {

    [Header("Use this to test performance at low and high framerates")]

	public bool lockFramerate;

    [Range(12f, 240f)]
    public int frameRate;

    void Update() {
		if (lockFramerate)
        	Application.targetFrameRate = frameRate;
		else
			Application.targetFrameRate = -1;

        if (Input.GetKey(KeyCode.Escape)) {
            Application.Quit();
        }
    }

    void OnApplicationQuit() {
        Application.targetFrameRate = -1;
    }
}
