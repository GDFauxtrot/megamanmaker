﻿using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelIO : MonoBehaviour {

/*
    // Current save file structure: //

    FILE
    {
        HEADER
        LEVEL
    }

    // Data types //

    HEADER
    {
        Identifier: "MMMAKER"
        Version: U8

        // FOR NOW, AS ENTITY IS NOT IMPLEMENTED YET
        Spawn (X): U16
        Spawn (Y): U16
    }

    LEVEL
    {
        Identifier: "LEVEL"
        Name: STRING
        Author: STRING
        Description: STRING
        Width (X):  U16
        Height (Y): U16
        BGTopColor: U24
        BGBotColor: U24
        Data: U16[Width * Height - Compression]
    }

    STRING
    {
        Length: U32
        Data: U8[Length]
    }
*/

    private static string BASE_SAVE_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
        + "\\MegaManMaker\\Levels\\";
    private string SAMPLE_SAVE_PATH; // can't be instantiated here, as Application.dataPath can only be accessed at runtime

    static byte CURRENT_VERSION = 1;

    PersistentInfo persistentInfo;

    void Start() {
        SAMPLE_SAVE_PATH = Application.dataPath + "\\Databases\\Resources\\SampleLevels\\";

        if (true) {//Camera.main.GetComponent<GameMenuManager>().pauseType == PauseMenuType.Play) {
            if (gameObject.scene.name == "TestSandbox") {
                List<GameObject> list = LoadSampleLevel("sample_cutman");
                GameObject blockParent = GameObject.Find("Blocks");
                foreach (GameObject obj in list) {
                    obj.transform.parent = blockParent.transform;
                }
            } else {
                GameObject pi = GameObject.Find("PersistentInfo");
                if (pi == null) {
                    Debug.LogError("PersistentInfo was not instantiated! Was this scene loaded without starting at Intro?");
                } else if (!persistentInfo.newBuildLevel) {
                    persistentInfo = pi.GetComponent<PersistentInfo>();
                    LevelLoadedData loadedData = LoadLevelFile(persistentInfo.GetComponent<PersistentInfo>().fileNameForNextPlayLoad);
                    if (loadedData == null) {
                        // TODO Go back to Intro with an error message! D:
                        return;
                    }
                    // Parent the blocks for the sake of keeping things in order
                    GameObject blockParent = GameObject.Find("Blocks");
                    foreach (GameObject obj in loadedData.loadedBlocks) {
                        obj.transform.parent = blockParent.transform;
                    }

                    if (persistentInfo.levelLoadedType == LevelLoadedType.PLAY) {
                        GameObject mm = GameObject.Find("MegaMan");

                        mm.GetComponent<PlayerController>().ForceTruePosition(new Vector3(
                            loadedData.spawn.x + mm.GetComponent<BoxCollider2D>().size.x/2,
                            loadedData.spawn.y + mm.GetComponent<BoxCollider2D>().size.y/2, 0f));
                        Camera.main.GetComponent<BackgroundDraw>().SetBackgroundColors(loadedData.topColor, loadedData.bottomColor);
                    }
                }
            }
        } else {

        }
        
    }

    public LevelLoadedData LoadLevelFile(string fileName) {
        LevelLoadedData loadedData = new LevelLoadedData();

        // Save folder doesn't exist? Set it up and return null, so the caller knows nothing was found
        if (!Directory.Exists(BASE_SAVE_PATH)) {
            Directory.CreateDirectory(BASE_SAVE_PATH);
            return null;
        }

        // GetFiles has an override for specifying file extension, but it's really unnecessary -
        // a Maker file is a Maker file if the first 7 bytes of the file says so.
        try {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(BASE_SAVE_PATH + fileName))) {
                // Speaking of which, check the first 7 bytes for our identifier.
                if (Encoding.ASCII.GetString(reader.ReadBytes(7)) != "MMMAKER") {
                    Debug.LogError("Found a file while initializing level info that is NOT valid file!");
                    return null;
                }
                byte version = reader.ReadByte();
                if (version == 1) {
                    // Version 1

                    // Next 2 ushorts are player start pos (XY), and then the "LEVEL" identifier
                    int spawnX = reader.ReadUInt16();
                    int spawnY = reader.ReadUInt16();
                    loadedData.spawn = new Vector2(spawnX, spawnY);

                    // Doesn't hurt to make sure we're in the right place
                    if (Encoding.ASCII.GetString(reader.ReadBytes(5)) != "LEVEL") {
                        Debug.LogError("Found an error while initializing a level (LEVEL Marker)!");
                        return null;
                    }

                    // Next lines take int at the beginning of the strings, and then reads the strings
                    // that many bytes long from the file.
                    int nameLength = reader.ReadInt32();
                    loadedData.levelName = Encoding.UTF8.GetString(reader.ReadBytes(nameLength));
                    int authorLength = reader.ReadInt32();
                    loadedData.levelAuthor = Encoding.UTF8.GetString(reader.ReadBytes(authorLength));
                    int descriptionLength = reader.ReadInt32();
                    loadedData.levelDescription = Encoding.UTF8.GetString(reader.ReadBytes(descriptionLength));

                    int xx = reader.ReadUInt16();
                    int yy = reader.ReadUInt16();
                    loadedData.levelSize = new Vector2(xx, yy);

                    if (persistentInfo.levelLoadedType == LevelLoadedType.PLAY)
                        Camera.main.GetComponent<FollowCam>().UpdateWorldMax(xx, yy);

                    loadedData.topColor = new Color(
                        reader.ReadByte() / 255,
                        reader.ReadByte() / 255,
                        reader.ReadByte() / 255);
                    loadedData.bottomColor = new Color(
                        reader.ReadByte() / 255,
                        reader.ReadByte() / 255,
                        reader.ReadByte() / 255);

                    loadedData.loadedBlocks = ReadLevelFromBinaryReader(reader, version, xx, yy);
                } else {
                    // Uhh... we don't support this version. We need to return null, so the caller knows something happened.
                    return null;
                }
                reader.Close();
            }
        } catch (EndOfStreamException exception) {
            // ...well, that was unexpected. This file must have been corrupted or tampered with,
            // so we may as well continue and skip it.
            Debug.LogError("While reading a file, an EndOfStringException was thrown! (" + exception.Source + ")");
        }

        return loadedData;
    }

    public void SaveLevelFile(string name, string author, string description, Color bgTop, Color bgBottom, Vector2 spawn) {
        // Get level size
        Vector2 max = Vector2.zero;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Block")) {
            if (obj.transform.position.x > max.x)
                max.x = obj.transform.position.x;
            if (obj.transform.position.y > max.y)
                max.y = obj.transform.position.y;
        }

        ushort xx = (ushort)(max.x / 16 + 1), yy = (ushort)(max.y / 16 + 1);

        if (!Directory.Exists(BASE_SAVE_PATH))
            Directory.CreateDirectory(BASE_SAVE_PATH);
        using (BinaryWriter writer = new BinaryWriter(File.Create(BASE_SAVE_PATH + GameMenuManager.SanitizeFileName(name) + ".mml"))) {
            // FILE WRITING HERE (uses the file structure described at the top)

            // HEADER
            writer.Write(Encoding.ASCII.GetBytes("MMMAKER"));
            writer.Write(CURRENT_VERSION);
            writer.Write(System.Convert.ToUInt16(spawn.x));
            writer.Write(System.Convert.ToUInt16(spawn.y));

            // LEVEL
            writer.Write(Encoding.ASCII.GetBytes("LEVEL"));
            writer.Write(Encoding.UTF8.GetByteCount(name));
            writer.Write(Encoding.UTF8.GetBytes(name));
            writer.Write(Encoding.UTF8.GetByteCount(author));
            writer.Write(Encoding.UTF8.GetBytes(author));
            writer.Write(Encoding.UTF8.GetByteCount(description));
            writer.Write(Encoding.UTF8.GetBytes(description));
            writer.Write(xx);
            writer.Write(yy);
            writer.Write(System.Convert.ToByte(bgTop.r * 255));
            writer.Write(System.Convert.ToByte(bgTop.g * 255));
            writer.Write(System.Convert.ToByte(bgTop.b * 255));
            writer.Write(System.Convert.ToByte(bgBottom.r * 255));
            writer.Write(System.Convert.ToByte(bgBottom.g * 255));
            writer.Write(System.Convert.ToByte(bgBottom.b * 255));

            WriteLevelToBinaryWriter(writer, max);

            writer.Close();
        }
    }

    // OLD AND HELPER METHODS BE HERE, MATEY //

    public static List<string[]> GetSavesInfo() {
        List<string[]> found = new List<string[]>();

        // Save folder doesn't exist? Set it up and return the empty dict, so the caller knows nothing was found
        if (!Directory.Exists(BASE_SAVE_PATH)) {
            Directory.CreateDirectory(BASE_SAVE_PATH);
            return found;
        }

        DirectoryInfo dir = new DirectoryInfo(BASE_SAVE_PATH);

        foreach (FileInfo file in dir.GetFiles()) {
            try {
                // Checking the extension (I've had multiple entries from using HXD on the MML's, which creates .bak files)
                if (file.Extension != ".mml")
                    continue;

                    using (BinaryReader reader = new BinaryReader(file.OpenRead())) {
                    // Check the first 7 bytes for our identifier.
                    if (Encoding.ASCII.GetString(reader.ReadBytes(7)) != "MMMAKER") {
                        Debug.LogError("Found a file while loading level info that is NOT a valid file!");
                        // Continue to the next file if it's not valid.
                        continue;
                    }
                    byte version = reader.ReadByte();
                    if (version == 1) {
                        // Version 1

                        // Next 2 ushorts are player start pos, and then the "LEVEL" identifier,
                        // so to get the file info, skip ahead 9 bytes.
                        reader.ReadBytes(9);

                        // Next lines take int at the beginning of the strings, and then reads the strings
                        // that many bytes long from the file.
                        int nameLength = reader.ReadInt32();
                        string name = Encoding.UTF8.GetString(reader.ReadBytes(nameLength));
                        int authorLength = reader.ReadInt32();
                        string author = Encoding.UTF8.GetString(reader.ReadBytes(authorLength));
                        int descriptionLength = reader.ReadInt32();
                        string description = Encoding.UTF8.GetString(reader.ReadBytes(descriptionLength));

                        found.Add(new string[] { file.Name, name, author, description });
                    } else {
                        // Uhh... we don't support this version. We should skip this file, it's probably a new
                        // one that this older build doesn't support.
                        continue;
                    }
                    reader.Close();
                }
            } catch (EndOfStreamException exception) {
                // ...well, that was unexpected. This file must have been corrupted or tampered with,
                // so we may as well continue and skip it.
                Debug.LogError("While reading a file, an EndOfStringException was thrown! (" + exception.Source + ")");
                continue;
            }
        }

        // Before you go, Mouth Moods by Neil Cicierega is an audible masterpiece. Just thought I'd let you know.
        return found;
    }

    public void SaveSampleLevel(string name) {
        // Get level size
        Vector2 max = Vector2.zero;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Block")) {
            if (obj.transform.position.x > max.x)
                max.x = obj.transform.position.x;
            if (obj.transform.position.y > max.y)
                max.y = obj.transform.position.y;
        }

        ushort xx = (ushort)(max.x / 16 + 1), yy = (ushort)(max.y / 16 + 1);

        using (BinaryWriter writer = new BinaryWriter(File.Create(SAMPLE_SAVE_PATH + name + ".txt"))) {
            writer.Write(Encoding.ASCII.GetBytes("MMMAKER"));   // Header
            writer.Write(System.Convert.ToByte(1));             // Version
            writer.Write(xx);                                   // World dimensions
            writer.Write(yy);
            writer.Write(System.Convert.ToUInt16(32));           // Player spawn
            writer.Write(System.Convert.ToUInt16(256));
            // This initial header specifies everything needed to begin a barebones game:
            //  * World dimensions (for reading level data and configuring the camera)
            //  * Player spawn (for placing the player in the world somewhere)

            // To implement RLE compression, we encode sequences of the same block as FF FF ID ID ## ## (2-byte hexes),
            // where FF FF is the compression identifier, ID ID is the block id, and ## ## is how many blocks.
            int blockCount = 0;
            ushort blockID = 0;

            for (int y = 0; y <= max.y; y += 16) {
                for (int x = 0; x <= max.x; x += 16) {
                    // Overlap at block center to avoid grabbing wrong blocks
                    Collider2D[] collisions = Physics2D.OverlapPointAll(new Vector2(x + 8, y + 8));
                    Collider2D collide = null;
                    foreach (Collider2D c in collisions) {
                        if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                            collide = c;
                            break;
                        }
                    }
                    if (!(blockCount == 0 && blockID == 0)) {
                        if (collide == null) {
                            if (blockID != 0) {
                                WriteBlocks(writer, blockCount, blockID);
                                blockCount = 0;
                            }
                        } else {
                            if (collide.gameObject.layer == LayerMask.NameToLayer("Block") && blockID != collide.GetComponent<Block>().blockID) {
                                WriteBlocks(writer, blockCount, blockID);
                                blockCount = 0;
                            }
                        }
                    }

                    if (collide == null) {
                        blockID = 0;
                    } else {
                        if (collide.gameObject.layer == LayerMask.NameToLayer("Block")) {
                            blockID = collide.GetComponent<Block>().blockID;
                        }
                    }
                    ++blockCount;
                }
            }

            // Fill in final block data
            if (!(blockCount == 0 && blockID == 0)) {
                WriteBlocks(writer, blockCount, blockID);
            }
        }
    }

    public void SaveLevelToFile(string name) {
        // Get level size
        Vector2 max = Vector2.zero;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Block")) {
            if (obj.transform.position.x > max.x)
                max.x = obj.transform.position.x;
            if (obj.transform.position.y > max.y)
                max.y = obj.transform.position.y;
        }

        ushort xx = (ushort)(max.x/16 + 1), yy = (ushort)(max.y/16 + 1);

        if (!Directory.Exists(BASE_SAVE_PATH))
            Directory.CreateDirectory(BASE_SAVE_PATH);
        using (BinaryWriter writer = new BinaryWriter(File.Create(BASE_SAVE_PATH + name + ".mml"))) {
            writer.Write(Encoding.ASCII.GetBytes("MMMAKER"));   // Header
            writer.Write(System.Convert.ToByte(1));             // Version
            writer.Write(xx);                                   // World dimensions
            writer.Write(yy);
            writer.Write(System.Convert.ToUInt16(32));           // Player spawn
            writer.Write(System.Convert.ToUInt16(256));
            // This initial header specifies everything needed to begin a barebones game:
            //  * World dimensions (for reading level data and configuring the camera)
            //  * Player spawn (for placing the player in the world somewhere)

            // To implement RLE compression, we encode sequences of the same block as FF FF ID ID ## ## (2-byte hexes),
            // where FF FF is the compression identifier, ID ID is the block id, and ## ## is how many blocks.
            int blockCount = 0;
            ushort blockID = 0;
            
            for (int y = 0; y <= max.y; y += 16) {
                for (int x = 0; x <= max.x; x += 16) {
                    // Overlap at block center to avoid grabbing wrong blocks
                    Collider2D[] collisions = Physics2D.OverlapPointAll(new Vector2(x + 8, y + 8));
                    Collider2D collide = null;
                    foreach (Collider2D c in collisions) {
                        if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                            collide = c;
                            break;
                        }
                    }
                    if (!(blockCount == 0 && blockID == 0)) {
                        if (collide == null) {
                            if (blockID != 0) {
                                WriteBlocks(writer, blockCount, blockID);
                                blockCount = 0;
                            }
                        } else {
                            if (collide.gameObject.layer == LayerMask.NameToLayer("Block") && blockID != collide.GetComponent<Block>().blockID) {
                                WriteBlocks(writer, blockCount, blockID);
                                blockCount = 0;
                            }
                        }
                    }

                    if (collide == null) {
                        blockID = 0;
                    } else {
                        if (collide.gameObject.layer == LayerMask.NameToLayer("Block")) {
                            blockID = collide.GetComponent<Block>().blockID;
                        }
                    }
                    ++blockCount;
                }
            }

            // Fill in final block data
            if (!(blockCount == 0 && blockID == 0)) {
                WriteBlocks(writer, blockCount, blockID);
            }
        }
    }
    
    private List<GameObject> LoadSampleLevel(string name) {
        List<GameObject> returnList = new List<GameObject>();

        TextAsset asset = Resources.Load<TextAsset>("SampleLevels/" + name);
        Stream stream = new MemoryStream(asset.bytes);
        using (BinaryReader reader = new BinaryReader(stream)) {
            if (Encoding.ASCII.GetString(reader.ReadBytes(7)) != "MMMAKER")
                throw new FileLoadException("File is not a Mega Man Maker level!", BASE_SAVE_PATH + name + ".mml");
            int version = reader.ReadByte();
            if (version == 1) {
                // Version 1 - next 4 ushorts are level dim and player pos
                int xx = reader.ReadUInt16();
                int yy = reader.ReadUInt16();
                Camera.main.GetComponent<FollowCam>().UpdateWorldMax(xx, yy);
                ushort playerX = reader.ReadUInt16();
                ushort playerY = reader.ReadUInt16();
                returnList = ReadLevelFromBinaryReader(reader, version, xx, yy);

            }
        }
        return returnList;
    }

    private List<GameObject> LoadLevelFromFile(string name) {
        List<GameObject> returnList = new List<GameObject>();

        if (!Directory.Exists(BASE_SAVE_PATH)) {
            Directory.CreateDirectory(BASE_SAVE_PATH);
        } else {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(BASE_SAVE_PATH + name + ".mml"))) {
                if (Encoding.ASCII.GetString(reader.ReadBytes(7)) != "MMMAKER")
                    throw new FileLoadException("File is not a Mega Man Maker level!", BASE_SAVE_PATH + name + ".mml");
                int version = reader.ReadByte();
                if (version == 1) {
                    // Version 1 - next 4 ushorts are level dim and player pos
                    int xx = reader.ReadUInt16();
                    int yy = reader.ReadUInt16();
                    Camera.main.GetComponent<FollowCam>().worldMax = new Vector2(xx + 16, yy + 16);
                    ushort playerX = reader.ReadUInt16();
                    ushort playerY = reader.ReadUInt16();
                    returnList = ReadLevelFromBinaryReader(reader, version, xx, yy);
                    
                }
            }
        }
        return returnList;
    }

    private List<GameObject> ReadLevelFromBinaryReader(BinaryReader reader, int version, int xx, int yy) {
        List<GameObject> returnList = new List<GameObject>();
        for (int i = 0; i < yy * xx; ++i) {
            ushort blockID = reader.ReadUInt16();

            // CHECK BLOCK MACROS:
            // FF FF == ushort.MaxValue == RLE compression
            if (blockID == ushort.MaxValue) {
                ushort ID = reader.ReadUInt16();
                int count = reader.ReadUInt16();

                if (ID != 0) {
                    for (int j = 0; j < count; ++j) {
                        returnList.Add(LoadBlock((i % xx) * 16, (i / xx) * 16, ID));
                        ++i;
                    }
                } else {
                    i += count;
                }
                --i;
            } else if (blockID != 0) {
                returnList.Add(LoadBlock((i % xx) * 16, (i / xx) * 16, blockID));
            }
        }
        return returnList;
    }

    private GameObject LoadBlock(int x, int y, ushort ID) {
        GameObject block = Instantiate(Resources.Load<GameObject>("Block"));
        block.transform.position = new Vector3(x, y, ID);
        block.GetComponent<Block>().SetupBlock(ID);
        return block;
    }

    private void WriteLevelToBinaryWriter(BinaryWriter writer, Vector2 levelMax) {
        // To implement RLE compression, we encode sequences of the same block as FF FF ID ID ## ## (2-byte hexes),
        // where FF FF is the compression identifier, ID ID is the block id, and ## ## is how many blocks.
        int blockCount = 0;
        ushort blockID = 0;

        for (int y = 0; y <= levelMax.y; y += 16) {
            for (int x = 0; x <= levelMax.x; x += 16) {
                // Overlap at block center to avoid grabbing wrong blocks
                Collider2D[] collisions = Physics2D.OverlapPointAll(new Vector2(x + 8, y + 8));
                Collider2D collide = null;
                foreach (Collider2D c in collisions) {
                    if (c.gameObject.layer == LayerMask.NameToLayer("Block")) {
                        collide = c;
                        break;
                    }
                }
                if (!(blockCount == 0 && blockID == 0)) {
                    if (collide == null) {
                        if (blockID != 0) {
                            WriteBlocks(writer, blockCount, blockID);
                            blockCount = 0;
                        }
                    } else {
                        if (collide.gameObject.layer == LayerMask.NameToLayer("Block") && blockID != collide.GetComponent<Block>().blockID) {
                            WriteBlocks(writer, blockCount, blockID);
                            blockCount = 0;
                        }
                    }
                }

                if (collide == null) {
                    blockID = 0;
                } else {
                    if (collide.gameObject.layer == LayerMask.NameToLayer("Block")) {
                        blockID = collide.GetComponent<Block>().blockID;
                    }
                }
                ++blockCount;
            }
        }

        // Fill in final block data
        if (!(blockCount == 0 && blockID == 0)) {
            WriteBlocks(writer, blockCount, blockID);
        }
    }

    private void WriteBlocks(BinaryWriter writer, int blockCount, ushort blockID) {
        if (blockCount > 3) {
            writer.Write(ushort.MaxValue);
            writer.Write(blockID);
            writer.Write(System.Convert.ToUInt16(blockCount));
        } else {
            for (int z = 0; z < blockCount; z++) {
                writer.Write(System.Convert.ToInt16(blockID));
            }
        }
    }
}
