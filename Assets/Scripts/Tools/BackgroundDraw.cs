﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundDraw : MonoBehaviour {

    //int r, g, b, brightness;
    int currentColor;

    static Color defaultColorTop = new Color(0.9f, 1f, 1f), defaultColorBottom = new Color(0.6f, 0.7f, 0.7f);
    public Color colorTop, colorBottom;

    void Start() {
        SetBackgroundColors(defaultColorTop, defaultColorBottom);

        //brightness = 200;
        //currentColor = Random.Range(0, 2);
        //if (currentColor == 0) {
        //    r = Random.Range(brightness, 255);
        //    g = brightness;
        //    b = brightness;
        //} else if (currentColor == 1) {
        //    g = Random.Range(brightness, 255);
        //    r = brightness;
        //    b = brightness;
        //} else {
        //    b = Random.Range(brightness, 255);
        //    r = brightness;
        //    g = brightness;
        //}
    }

    public void SetBackgroundColors(Color top, Color bottom) {
        colorTop = top;
        colorBottom = bottom;
        Texture2D bg = new Texture2D(1, 2, TextureFormat.ARGB32, false);
        bg.SetPixel(0, 1, top);
        bg.SetPixel(0, 0, bottom);
        bg.filterMode = FilterMode.Bilinear;
        bg.Apply();

        Sprite spr = Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0.5f, 0.5f));
        GameObject.Find("BackgroundImage").GetComponent<SpriteRenderer>().sprite = spr;
    }

    void FixedUpdate() {
        //if (currentColor == 0) {
        //    if (r == 255 && b == brightness) {
        //        currentColor = 1;
        //    } else {
        //        if (r < 255)
        //            ++r;
        //        else
        //            --b;
        //    }
        //} else if (currentColor == 1) {
        //    if (g == 255 && r == brightness) {
        //        currentColor = 2;
        //    } else {
        //        if (g < 255)
        //            ++g;
        //        else
        //            --r;
        //    }
        //} else if (currentColor == 2) {
        //    if (b == 255 && g == brightness) {
        //        currentColor = 0;
        //    } else {
        //        if (b < 255)
        //            ++b;
        //        else
        //            --g;
        //    }
        //}

        //Texture2D bg = new Texture2D(1, 2, TextureFormat.ARGB32, false);
        //bg.SetPixel(0, 1, new Color(r / 255f, g / 255f, b / 255f));
        //bg.SetPixel(0, 0, new Color(r / 255f, g / 255f, b / 255f));
        ////bg.filterMode = FilterMode.Bilinear;
        //bg.Apply();

        //Sprite spr = Sprite.Create(bg, new Rect(0, 0, bg.width, bg.height), new Vector2(0.5f, 0.5f));
        //GameObject.Find("BackgroundImage").GetComponent<SpriteRenderer>().sprite = spr;
    }
}
