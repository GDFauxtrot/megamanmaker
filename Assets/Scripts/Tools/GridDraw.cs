﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridDraw : MonoBehaviour {
    static Material lineMaterial;
    static Color lightGrey = new Color(0.6f, 0.6f, 0.6f, 1.0f);

    void Start() {
    }
	
	void Update() {
	}

    static void CreateLineMaterial() {
        if (!lineMaterial) {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    void OnPostRender() {
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        int levelMax = 65535;
        int gridRadius = 64;

        // Draw lines
        GL.Begin(GL.LINES);

        for (int x = -gridRadius; x < gridRadius; ++x) {
            // Every major (4th) grid line is black, rest are a light grey
            if ((x + Mathf.Round(transform.position.x / 16.0f)) % 4 == 0)
                GL.Color(Color.black);
            else
                GL.Color(lightGrey);

            GL.Vertex3(16 * (x + Mathf.Round(transform.position.x / 16.0f)) - transform.position.x, transform.position.y - levelMax, 0);
            GL.Vertex3(16 * (x + Mathf.Round(transform.position.x / 16.0f)) - transform.position.x, transform.position.y + levelMax, 0);
        }
        for (int y = -gridRadius; y < gridRadius; ++y) {
            // Every major (4th) grid line is black, rest are a light grey
            if ((y + Mathf.Round(transform.position.y / 16.0f)) % 4 == 0)
                GL.Color(Color.black);
            else
                GL.Color(lightGrey);

            GL.Vertex3(transform.position.x - levelMax, 16 * (y + Mathf.Round(transform.position.y / 16.0f)) - transform.position.y, 0);
            GL.Vertex3(transform.position.x + levelMax, 16 * (y + Mathf.Round(transform.position.y / 16.0f)) - transform.position.y, 0);
        }

        GL.End();
        
        GL.Begin(GL.QUADS);

        GL.Color(new Color(0, 0, 0, 0.5f));

        // X
        GL.Vertex3(-transform.position.x, levelMax, 0);
        GL.Vertex3(-transform.position.x - levelMax, levelMax, 0);
        GL.Vertex3(-transform.position.x - levelMax, -transform.position.y, 0);
        GL.Vertex3(-transform.position.x, -transform.position.y, 0);
        
        // Y
        GL.Vertex3(levelMax, -transform.position.y, 0);
        GL.Vertex3(-levelMax, -transform.position.y, 0);
        GL.Vertex3(levelMax, -transform.position.y - levelMax, 0);
        GL.Vertex3(-levelMax, -transform.position.y - levelMax, 0);

        GL.End();

        GL.PopMatrix();
    }
}
