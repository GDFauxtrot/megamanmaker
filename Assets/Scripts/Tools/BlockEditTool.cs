﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEditTool : MonoBehaviour {

	GameObject ghost;
    private ushort ID;

	void Start () {
        ID = 1;
		ghost = new GameObject("Ghost");
		ghost.layer = LayerMask.NameToLayer("UI");
		ghost.transform.localScale = new Vector3(4f, 4f, ghost.transform.position.z);
		ghost.AddComponent<SpriteRenderer>();
		ghost.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(ID), "ghost", -8, -8);
        ghost.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Foreground");
        Color color = ghost.GetComponent<SpriteRenderer>().color;
		color.a = 0.5f;
		ghost.GetComponent<SpriteRenderer>().color = color;
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            if (BlockID.isValidID(ID+1))
                ++ID;
            //GameObject.Find("Palette").GetComponent<PaletteManager>().nextIndex();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            if (ID > 1)
                --ID;
            //GameObject.Find("Palette").GetComponent<PaletteManager>().prevIndex();
        }
        ghost.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(ID), "ghost", -8, -8);
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector2 blockPos = new Vector2(Mathf.Floor(mousePos.x/16)*16, Mathf.Floor(mousePos.y/16)*16);
        Vector2 blockPos2 = new Vector2(blockPos.x + 8,blockPos.y + 8);
        ghost.transform.position = new Vector3(blockPos.x, blockPos.y, transform.position.z);

		if (Input.GetMouseButtonDown(0)) {
            Collider2D col = Physics2D.OverlapPoint(blockPos2);
			
			if (col == null) {
				GameObject newBlock = Instantiate(Resources.Load<GameObject>("Block"));
				newBlock.transform.position = new Vector3(blockPos.x, blockPos.y, ID);
                newBlock.GetComponent<Block>().SetupBlock(ID);
				newBlock.GetComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite(BlockID.GetTextureLocation(ID), "ghost", -8, -8);
                newBlock.transform.parent = GameObject.Find("Blocks").transform;
                StaticBatchingUtility.Combine(GameObject.Find("Blocks"));
			} else {
				if (col.gameObject.layer == LayerMask.NameToLayer("Block")
					&& col.gameObject.name != "Ladder" && col.gameObject.name.Contains("Clone"))
					Destroy(col.gameObject);
			}
		}
	}
}
