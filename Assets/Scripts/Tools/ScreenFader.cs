﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ATTACH ME TO A GAMEOBJECT!! (k thanks)

public class ScreenFader : MonoBehaviour {

    private Coroutine currentFade;
    //private Texture2D colorTex;
    //private GUIStyle gui;

    // Kludgy way of fading the screen, but we need to be able to
    // draw UI on top (OnGUI() and shaders won't do)
    private GameObject fadeObject;

    void Awake() {
        if (fadeObject == null) {
            fadeObject = new GameObject("Fade", typeof(Image));
            fadeObject.transform.SetParent(GameObject.Find("Canvas").transform, false);
            fadeObject.transform.SetSiblingIndex(fadeObject.transform.GetSiblingIndex() - 1);
            fadeObject.GetComponent<RectTransform>().anchorMin = Vector2.zero;
            fadeObject.GetComponent<RectTransform>().anchorMax = Vector2.one;
            fadeObject.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
            fadeObject.GetComponent<Image>().raycastTarget = false;
        }
        fadeObject.GetComponent<Image>().color = Vector4.zero;

        //colorTex = new Texture2D(1, 1);
        //colorTex.SetPixel(0, 0, Vector4.zero); // Vector4.zero = black, 100% transparent
        //colorTex.Apply();
        //gui = new GUIStyle();
        //gui.normal.background = colorTex;
    }

    //void OnGUI() {
        //GUI.Box(new Rect(0, 0, Screen.width, Screen.height), GUIContent.none, gui);
    //}

    public void ClearColor() {
        //colorTex.SetPixel(0, 0, Vector4.zero);
        //colorTex.Apply();
        fadeObject.GetComponent<Image>().color = Vector4.zero;
        if (currentFade != null) {
            StopCoroutine(currentFade);
        }
    }

    public void FadeFrom(Color color, float seconds, bool useFixedDeltaTime = false) {
        if (currentFade != null) {
            StopCoroutine(currentFade);
        }
        currentFade = StartCoroutine(Fade(color, Vector4.zero, seconds, useFixedDeltaTime));
    }

    public void FadeTo(Color color, float seconds, bool useFixedDeltaTime = false) {
        if (currentFade != null) {
            StopCoroutine(currentFade);
        }
        currentFade = StartCoroutine(Fade(Vector4.zero, color, seconds, useFixedDeltaTime));
    }

    public void FadeFromTo(Color from, Color to, float seconds, bool useFixedDeltaTime = false) {
        if (currentFade != null) {
            StopCoroutine(currentFade);
        }
        currentFade = StartCoroutine(Fade(from, to, seconds, useFixedDeltaTime));
    }

    private IEnumerator Fade(Color colorFrom, Color colorTo, float seconds, bool useFixedDeltaTime) {
        float startTime = Time.time;
        while (Time.time < startTime + seconds) {
            float ratio = (Time.time - startTime) / seconds;
            //color.a = alpha * amount;
            fadeObject.GetComponent<Image>().color = Color.Lerp(colorFrom, colorTo, ratio);
            //colorTex.SetPixel(0, 0, color);
            //colorTex.Apply();

            if (useFixedDeltaTime) {
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            } else {
                yield return new WaitForSeconds(Time.deltaTime);
            }
        }

        fadeObject.GetComponent<Image>().color = colorTo;
    }
}
