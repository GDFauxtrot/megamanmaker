﻿using UnityEngine;
using System.Collections.Generic;

public class Texture2DImporter : MonoBehaviour {

	public static Sprite LoadTexture2DToSprite(string texname, string sprname, float xPixelOffset = 0f, float yPixelOffset = 0f) {
        
        Texture2D t2d = (Texture2D)Resources.Load(texname);
        if (t2d == null) {
            Debug.LogError("Cannot find texture '" + texname + "' !");
        }
        Rect size = new Rect(0, 0, t2d.width, t2d.height);
        Vector2 pivot = new Vector2(0.5f + xPixelOffset / t2d.width, 0.5f + yPixelOffset / t2d.height);
        Sprite sprite = Sprite.Create(t2d, size, pivot, 4);
        sprite.name = sprname;

        return sprite;
    }

    public static Sprite LoadBlockSprite(string texname) {
        Texture2D t2d = (Texture2D)Resources.Load(texname);
        if (t2d == null) {
            Debug.LogError("Cannot find texture '" + texname + "' !");
        }
        Sprite sprite = Sprite.Create(t2d, new Rect(0, 0, t2d.width, t2d.height), Vector2.zero, 4);
        string[] split = texname.Split('/');
        sprite.name = split[split.Length - 1];
        return sprite;
    }
}
