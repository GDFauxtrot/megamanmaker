﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SetupScene : MonoBehaviour {

    public bool forceReset;
    public int introTextIterations;

    public GameObject titleText, titleVersion, titleCredit;
    public GameObject playButton, buildButton, exitButton;
    public GameObject levelEntries, levelBackButton;

    public GameObject menuPlayButton, menuBuildButton;

    private float introSpeed;
    private float startTime;
    private int towerSize;

    private Sprite t1, t2, bg;
    private GameObject background, tower;
    private float finalTowerY, finalBackgroundY;
    private float backgroundPercentage, movingPercentage;
    private Coroutine movingCoroutine, logoCoroutine;
    private Coroutine levelPanelCoroutine, levelPanelReturnCoroutine;
    private bool levelPanelShowing;
    private bool logoCoroutineRan;
    private ScreenFaderManager screenFader;

    PersistentInfo persistentInfo;

    void Start() {
        movingPercentage = 0f;
        backgroundPercentage = 0.8f;
        towerSize = 34;
        BuildScene();
        introSpeed = 50f;
        startTime = Time.time;
        finalTowerY = tower.transform.position.y - t1.texture.height * (towerSize - 2);
        finalBackgroundY = background.transform.position.y - bg.texture.height * backgroundPercentage;
        movingCoroutine = StartCoroutine(Intro());

        persistentInfo = GameObject.Find("PersistentInfo").GetComponent<PersistentInfo>();

        screenFader = GameObject.Find("ScreenFaderManager").GetComponent<ScreenFaderManager>();
        screenFader.FadeFrom(Color.black, 2f);
    }

    void Update() {
        if (forceReset) {
            forceReset = false;
            screenFader.FadeFrom(Color.black, 2f);
            BuildScene();
            startTime = Time.time;
            finalTowerY = tower.transform.position.y - t1.texture.height * (towerSize - 2);
            finalBackgroundY = background.transform.position.y - bg.texture.height * backgroundPercentage;
            StopCoroutine(movingCoroutine);
            movingCoroutine = StartCoroutine(Intro());
            StopCoroutine(logoCoroutine);
            logoCoroutineRan = false;
        }

        if (Input.anyKeyDown && background.transform.position.y != finalBackgroundY && tower.transform.position.y != finalTowerY) {
            StopCoroutine(movingCoroutine);
            background.transform.position = new Vector3(background.transform.position.x, finalBackgroundY, background.transform.position.z);
            tower.transform.position = new Vector3(tower.transform.position.x, finalTowerY, tower.transform.position.z);
            screenFader.ClearColor();
        }
        
        if ((movingPercentage > 0.9f || Input.anyKeyDown) && !logoCoroutineRan) {
            logoCoroutine = StartCoroutine(IntroLogo());
        }

        if (Input.GetKeyDown(KeyCode.Escape) && levelPanelShowing) {
            if (levelPanelReturnCoroutine != null) {
                StopCoroutine(levelPanelReturnCoroutine);
            }
            levelPanelReturnCoroutine = StartCoroutine(ExitLevelSelectionPanel());
        }
    }

    private IEnumerator Intro() {
        float initialTowerY = tower.transform.position.y;
        while (tower.transform.position.y > finalTowerY + 0.1) {
            float timeElapsed = Time.time - startTime;
            float percentage = timeElapsed / introSpeed;
            if (percentage < 0.5f) {
                tower.transform.position = new Vector3(
                    tower.transform.position.x,
                    Mathf.Lerp(tower.transform.position.y, finalTowerY, timeElapsed / introSpeed / 2),
                    tower.transform.position.z);
                background.transform.position = new Vector3(
                    background.transform.position.x,
                    Mathf.Lerp(background.transform.position.y, finalBackgroundY, timeElapsed / introSpeed / 2),
                    background.transform.position.z);
            } else {
                tower.transform.position = new Vector3(
                    tower.transform.position.x,
                    Mathf.SmoothStep(tower.transform.position.y, finalTowerY, timeElapsed / introSpeed),
                    tower.transform.position.z);
                background.transform.position = new Vector3(
                    background.transform.position.x,
                    Mathf.SmoothStep(background.transform.position.y, finalBackgroundY, timeElapsed / introSpeed),
                    background.transform.position.z);
            }
            movingPercentage = (tower.transform.position.y - initialTowerY) / (finalTowerY - initialTowerY);
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        background.transform.position = new Vector3(background.transform.position.x, finalBackgroundY, background.transform.position.z);
        tower.transform.position = new Vector3(tower.transform.position.x, finalTowerY, tower.transform.position.z);
    }

    private IEnumerator IntroLogo() {
        logoCoroutineRan = true;
        for (int x = 0; x < introTextIterations + 1; x++) {
            if (titleText.GetComponent<Text>().color.a == 0f) {
                titleText.GetComponent<Text>().color = Color.black;
            } else {
                titleText.GetComponent<Text>().color = new Color(
                    titleText.GetComponent<Text>().color.r + (255f / introTextIterations) / 255f,
                    titleText.GetComponent<Text>().color.g + (255f / introTextIterations) / 255f,
                    titleText.GetComponent<Text>().color.b + (255f / introTextIterations) / 255f);
            }
            titleVersion.GetComponent<Text>().color = titleText.GetComponent<Text>().color;
            titleCredit.GetComponent<Text>().color = titleText.GetComponent<Text>().color;
            if (x / ((float)introTextIterations) > 0.5f && playButton != null && buildButton != null && exitButton != null) {
                playButton.GetComponent<PlayButton>().MoveIn();
                buildButton.GetComponent<BuildButton>().MoveIn();
                exitButton.GetComponent<ExitButton>().MoveIn();
            }
            yield return new WaitForSeconds(1.5f / introTextIterations);
        }
    }
    
    public void BuildScene() {
        if (GameObject.Find("Background") != null) {
            Destroy(GameObject.Find("Background"));
        }
        if (GameObject.Find("Tower") != null) {
            Destroy(GameObject.Find("Tower"));
        }

        t1 = Texture2DImporter.LoadTexture2DToSprite("MainMenu/tower-0", "tower-0");
        t2 = Texture2DImporter.LoadTexture2DToSprite("MainMenu/tower-1", "tower-1");
        bg = Texture2DImporter.LoadTexture2DToSprite("MainMenu/background", "background");
        Sprite balcony = Texture2DImporter.LoadTexture2DToSprite("MainMenu/balcony", "balcony");

        float h = Camera.main.orthographicSize;
        float w = h * Screen.width / Screen.height;

        // Setup background

        background = new GameObject("Background");
        background.transform.position = new Vector3(0f, bg.texture.height / 2f - h);
        background.transform.localScale = new Vector3(4f, 4f, 1f);
        int count = Mathf.CeilToInt(w / (bg.texture.width / 2f));
        for (int i = 0; i < count; i++) {
            GameObject go = new GameObject("background" + i.ToString());
            go.transform.parent = background.transform;
            go.AddComponent<SpriteRenderer>();
            go.GetComponent<SpriteRenderer>().sprite = bg;
            go.GetComponent<SpriteRenderer>().sortingLayerName = "Background";
            go.transform.localPosition = new Vector3((-bg.texture.width / 8f) * (count - 1) + (bg.texture.width / 4f) * i, 0f);
            go.transform.localScale = Vector3.one;
        }

        // Setup Tower

        tower = new GameObject("Tower");
        tower.transform.position = new Vector3(w - t1.texture.width / 2, t1.texture.height / 2 - h);
        tower.transform.localScale = new Vector3(4f, 4f, 1f);
        for (int i = 0; i < towerSize; i++) {
            GameObject go = new GameObject("tower" + i.ToString());
            go.AddComponent<SpriteRenderer>();
            go.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            go.transform.parent = tower.transform;
            if (i % 2 == 0 || i == towerSize-1) {
                go.GetComponent<SpriteRenderer>().sprite = t1;
                go.transform.localPosition = new Vector3(0f, t1.texture.height / 4f * i);
                go.transform.localScale = Vector3.one;
                if (i != towerSize - 1) {
                    GameObject balc = new GameObject("balcony" + (i / 2).ToString());
                    balc.AddComponent<SpriteRenderer>();
                    balc.GetComponent<SpriteRenderer>().sprite = balcony;
                    balc.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
                    balc.transform.parent = tower.transform;
                    balc.transform.localPosition = new Vector3(-t1.texture.width / 8f - balcony.texture.width / 8f, t1.texture.height / 4f * i);
                    balc.transform.localScale = Vector3.one;
                }
            } else {
                go.GetComponent<SpriteRenderer>().sprite = t2;
                go.transform.localPosition = new Vector3(0f, t2.texture.height / 4 * i);
                go.transform.localScale = Vector3.one;
            }
        }

        // Setting up top of tower

        for (int i = 0; i < 5; i++) {
            GameObject towerTop = new GameObject("tower-top" + i.ToString());
            Sprite spr = Texture2DImporter.LoadTexture2DToSprite("MainMenu/tower-top", "tower-top");
            towerTop.AddComponent<SpriteRenderer>().sprite = spr;
            towerTop.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            towerTop.transform.parent = tower.transform;
            towerTop.transform.localPosition = new Vector3(-spr.texture.width / 8f * (3 - i*2),
                t1.texture.height * towerSize / 4 - 2f);
            towerTop.transform.localScale = Vector3.one;
        }

        {
            GameObject towerCap = new GameObject("tower-cap");
            towerCap.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/roof-cap", "roof-cap");
            towerCap.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            towerCap.transform.parent = tower.transform;
            towerCap.transform.localPosition = new Vector3(-towerCap.GetComponent<SpriteRenderer>().sprite.texture.width / 8f * 5,
                t1.texture.height * towerSize / 4 - 1f);
            towerCap.transform.localScale = Vector3.one;
        }

        for (int i = 0; i < 3; i++) {
            GameObject vertWall = new GameObject("roof-vert" + i.ToString());
            vertWall.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/roof-vert", "roof-vert");
            vertWall.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            vertWall.transform.parent = tower.transform;
            vertWall.transform.localPosition = new Vector3(-vertWall.GetComponent<SpriteRenderer>().sprite.texture.width / 8f * 3,
                t1.texture.height * towerSize / 4 + vertWall.GetComponent<SpriteRenderer>().sprite.texture.height / 4 * (i + 0.5f));
            vertWall.transform.localScale = Vector3.one;
        }

        {
            GameObject cornerWall = new GameObject("roof-corner");
            cornerWall.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/roof-corner", "roof-corner");
            cornerWall.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            cornerWall.transform.parent = tower.transform;
            cornerWall.transform.localPosition = new Vector3(-cornerWall.GetComponent<SpriteRenderer>().sprite.texture.width / 8f * 3,
                t1.texture.height * towerSize / 4 + cornerWall.GetComponent<SpriteRenderer>().sprite.texture.height / 4 * 3.5f);
            cornerWall.transform.localScale = Vector3.one;
        }

        for (int i = 0; i < 7; i++) {
            GameObject horizWall = new GameObject("roof-horiz" + i.ToString());
            horizWall.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/roof-horiz", "roof-horiz");
            horizWall.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            horizWall.transform.parent = tower.transform;
            horizWall.transform.localPosition = new Vector3(horizWall.GetComponent<SpriteRenderer>().sprite.texture.width / 4 * i - 1,
                t1.texture.height * towerSize / 4 + horizWall.GetComponent<SpriteRenderer>().sprite.texture.height / 4 * 3.5f);
            horizWall.transform.localScale = Vector3.one;
        }

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 3; j++) {
                GameObject wall = new GameObject("roof-wall" + i.ToString() + "-" + j.ToString());
                wall.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/roof-wall", "roof-wall");
                wall.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
                wall.transform.parent = tower.transform;
                wall.transform.localPosition = new Vector3(wall.GetComponent<SpriteRenderer>().sprite.texture.width / 4 * i - 1,
                    t1.texture.height * towerSize / 4 + wall.GetComponent<SpriteRenderer>().sprite.texture.height / 4 * (j + 0.5f));
                wall.transform.localScale = Vector3.one;
            }
        }

        {
            GameObject door = new GameObject("door");
            door.AddComponent<SpriteRenderer>().sprite = Texture2DImporter.LoadTexture2DToSprite("MainMenu/door", "door");
            door.GetComponent<SpriteRenderer>().sortingLayerName = "Front Foreground";
            door.GetComponent<SpriteRenderer>().sortingOrder = 1;
            door.transform.parent = tower.transform;
            door.transform.localPosition = new Vector3(door.GetComponent<SpriteRenderer>().sprite.texture.width / 4 + 2f,
                t1.texture.height * towerSize / 4 + door.GetComponent<SpriteRenderer>().sprite.texture.height / 4 - 2f);
            door.transform.localScale = Vector3.one;
        }
    }

    public void ShowPlayLevelSelection() {
        screenFader.FadeTo(new Color(0f,0f,0f,0.5f), 1f);
        playButton.GetComponent<Button>().enabled = false;
        buildButton.GetComponent<Button>().enabled = false;
        exitButton.GetComponent<Button>().enabled = false;
        levelPanelShowing = true;
        if (levelPanelCoroutine != null) {
            StopCoroutine(levelPanelCoroutine);
        }
        levelPanelCoroutine = StartCoroutine(ShowLevelSelection(1000f, 0f, 1f));

        menuPlayButton.SetActive(true);
        menuBuildButton.SetActive(false);
        levelEntries.GetComponent<LevelEntryManager>().SetupLocalEntries(false);
    }

    public void ShowBuildLevelSelection() {
        screenFader.FadeTo(new Color(0f, 0f, 0f, 0.5f), 1f);
        playButton.GetComponent<Button>().enabled = false;
        buildButton.GetComponent<Button>().enabled = false;
        exitButton.GetComponent<Button>().enabled = false;
        levelPanelShowing = true;
        if (levelPanelCoroutine != null) {
            StopCoroutine(levelPanelCoroutine);
        }
        levelPanelCoroutine = StartCoroutine(ShowLevelSelection(1000f, 0f, 1f));

        menuPlayButton.SetActive(false);
        menuBuildButton.SetActive(true);

        levelEntries.GetComponent<LevelEntryManager>().SetupLocalEntries(true);
    }

    public void StartLevel() {
        playButton.GetComponent<Button>().enabled = false;
        buildButton.GetComponent<Button>().enabled = false;
        exitButton.GetComponent<Button>().enabled = false;
        StartCoroutine(StartLevelTransition());
    }

    public void StartEditor() {
        playButton.GetComponent<Button>().enabled = false;
        buildButton.GetComponent<Button>().enabled = false;
        exitButton.GetComponent<Button>().enabled = false;
        StartCoroutine(BuildLevelTransition(false));
    }

    public void StartNewEditorLevel() {
        playButton.GetComponent<Button>().enabled = false;
        buildButton.GetComponent<Button>().enabled = false;
        exitButton.GetComponent<Button>().enabled = false;
        StartCoroutine(BuildLevelTransition(true));
    }

    private IEnumerator ExitLevelSelectionPanel() {
        levelPanelShowing = false;
        if (levelPanelCoroutine != null) {
            StopCoroutine(levelPanelCoroutine);
        }
        levelPanelCoroutine = StartCoroutine(ShowLevelSelection(GameObject.Find("LevelSelectPanel").GetComponent<RectTransform>().localPosition.y, 1000f, 1f));
        screenFader.FadeFrom(new Color(0f, 0f, 0f, 0.5f), 1f);
        yield return new WaitForSeconds(1f);
        GameObject.Find("LevelEntries").GetComponent<LevelEntryManager>().ClearSelectedItems();
        playButton.GetComponent<Button>().enabled = true;
        buildButton.GetComponent<Button>().enabled = true;
        exitButton.GetComponent<Button>().enabled = true;
    }

    private IEnumerator ShowLevelSelection(float yFrom, float yTo, float time) {
        GameObject levelPanel = GameObject.Find("LevelSelectPanel");
        float startTime = Time.time;
        while (Time.time < startTime + time) {
            float ratio = (Time.time - startTime) / time;
            levelPanel.GetComponent<RectTransform>().localPosition = new Vector3(
                levelPanel.GetComponent<RectTransform>().localPosition.x,
                Mathf.SmoothStep(yFrom, yTo, ratio), levelPanel.GetComponent<RectTransform>().localPosition.z);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        levelPanel.transform.localPosition = new Vector3(
                levelPanel.transform.localPosition.x,
                yTo, transform.localPosition.z);
    }

    private IEnumerator StartLevelTransition() {
        LevelEntryItem selectedLevelEntry = GameObject.Find("LevelEntries").GetComponent<LevelEntryManager>().GetSelectedLevelEntryItem();
        if (selectedLevelEntry == null) {
            yield break;
        }

        if (levelPanelCoroutine != null) {
            StopCoroutine(levelPanelCoroutine);
        }
        levelPanelCoroutine = StartCoroutine(ShowLevelSelection(0f, 1000f, 1f));
        screenFader.FadeFromTo(new Color(0f,0f,0f,0.5f), Color.black, 1.5f);

        persistentInfo.fileNameForNextPlayLoad = selectedLevelEntry.levelFileName;
        persistentInfo.levelLoadedType = LevelLoadedType.PLAY;
        persistentInfo.newBuildLevel = false;
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Play");
    }

    private IEnumerator BuildLevelTransition(bool newLevel) {
        LevelEntryItem selectedLevelEntry = GameObject.Find("LevelEntries").GetComponent<LevelEntryManager>().GetSelectedLevelEntryItem();

        if (!newLevel && selectedLevelEntry == null) {
            yield break;
        }
        if (levelPanelCoroutine != null) {
            StopCoroutine(levelPanelCoroutine);
        }

        levelPanelCoroutine = StartCoroutine(ShowLevelSelection(0f, 1000f, 1f));
        screenFader.FadeFromTo(new Color(0f, 0f, 0f, 0.5f), Color.black, 1.5f);

        if (newLevel) {
            persistentInfo.newBuildLevel = true;
        } else {
            persistentInfo.fileNameForNextPlayLoad = selectedLevelEntry.levelFileName;
            persistentInfo.newBuildLevel = false;
        }
        persistentInfo.levelLoadedType = LevelLoadedType.BUILD;
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Build");
    }
}
