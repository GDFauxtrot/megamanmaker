﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewLevelEntryItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler {

    bool isPressed, isSelected, isHovered;

    float lerpTime;

    int id;

    //public string levelFileName, levelName, levelAuthor, levelDescription;

    static Color selected = new Color(0.6f, 0.6f, 0.6f),
        unselected = new Color(0.75f, 0.75f, 0.75f),
        highlighted = new Color(0.85f, 0.85f, 0.85f),
        pressed = new Color(0.5f, 0.5f, 0.5f);

    void Start() {
    }

    void Update() {
        if (lerpTime < 2f) {
            lerpTime += Time.deltaTime;

            float lerp = lerpTime / 0.5f;

            Image bg = gameObject.transform.GetChild(0).GetComponent<Image>();

            if (isPressed) {
                bg.color = new Color(
                    Mathf.Lerp(bg.color.r, pressed.r, lerp),
                    Mathf.Lerp(bg.color.g, pressed.g, lerp),
                    Mathf.Lerp(bg.color.b, pressed.b, lerp));
            } else if (isSelected) {
                bg.color = new Color(
                    Mathf.Lerp(bg.color.r, selected.r, lerp),
                    Mathf.Lerp(bg.color.g, selected.g, lerp),
                    Mathf.Lerp(bg.color.b, selected.b, lerp));
            } else if (isHovered) {
                bg.color = new Color(
                    Mathf.Lerp(bg.color.r, highlighted.r, lerp),
                    Mathf.Lerp(bg.color.g, highlighted.g, lerp),
                    Mathf.Lerp(bg.color.b, highlighted.b, lerp));
            } else {
                bg.color = new Color(
                    Mathf.Lerp(bg.color.r, unselected.r, lerp),
                    Mathf.Lerp(bg.color.g, unselected.g, lerp),
                    Mathf.Lerp(bg.color.b, unselected.b, lerp));
            }
        }
    }

    /*
    public void SetupEntry(int ID, string[] info) {
        id = ID;

        levelFileName = info[0];
        levelName = info[1];
        levelAuthor = info[2];
        levelDescription = info[3];

        gameObject.transform.GetChild(1).GetComponent<Text>().text = levelName;
        gameObject.transform.GetChild(2).GetComponent<Text>().text = levelAuthor;
    }
    */

    public void setSelected(bool selected) {
        isSelected = selected;
        lerpTime = 0f;
    }

    public void OnPointerEnter(PointerEventData eventData) {
        isHovered = true;
        lerpTime = 0f;
        if (eventData.eligibleForClick) {
            isPressed = true;
            gameObject.transform.parent.GetComponent<LevelEntryManager>().SetSelectedLevelID(id);
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        isHovered = false;
        lerpTime = 0f;
        if (isPressed) {
            isPressed = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        isPressed = true;
        lerpTime = 0f;
    }

    public void OnPointerUp(PointerEventData eventData) {
        isPressed = false;
        lerpTime = 0f;
        if (isHovered) {
            GameObject.Find("Setup").GetComponent<SetupScene>().StartNewEditorLevel();
        }
    }
}
