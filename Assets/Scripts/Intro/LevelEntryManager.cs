﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelEntryManager : MonoBehaviour {

    public GameObject levelEntryPrefab, newLevelPrefab;

    public GameObject selectedName, selectedAuthor, selectedDescription;
    public GameObject scrollBar;
    public GameObject emptyListText;
    public GameObject playButton;
    public GameObject buildButton;

    Dictionary<int, GameObject> levels;

    int selectedLevel = -1;

    float entrySize = 150f;
    float yMax;

    RectTransform parentRect;

    void Start() {
        levels = new Dictionary<int, GameObject>();
        parentRect = transform.parent.GetComponent<RectTransform>();
    }

    /// <summary>
    /// Call this function to set up the entry list when pulling up the level select menu.
    /// </summary>
    public void SetupLocalEntries(bool buildMode) {
        if (levels != null) {
            foreach (GameObject entry in levels.Values) {
                Destroy(entry);
            }
        }

        levels = new Dictionary<int, GameObject>();

        // Grab all the found levels using LevelIO
        List<string[]> foundLevels = LevelIO.GetSavesInfo();

        // Nothing was found, so don't show anything
        if (foundLevels.Count == 0) {
            gameObject.transform.parent.Find("EmptyListText").gameObject.SetActive(true);
            return;
        } else {
            gameObject.transform.parent.Find("EmptyListText").gameObject.SetActive(false);
        }

        float yy = 0;
        int ID = 0;

        float sizeY = 0;

        foreach (string[] info in foundLevels) {
            GameObject entry = Instantiate(levelEntryPrefab);

            entry.transform.SetParent(gameObject.transform);
            entry.transform.position = Vector2.zero;
            entry.transform.localScale = Vector3.one;

            yy = (GetComponent<RectTransform>().rect.y + entrySize/2f) + (ID * entrySize);
            sizeY += entrySize;
            entry.GetComponent<RectTransform>().localPosition = new Vector3(0f, -yy, 0);
            entry.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, entry.GetComponent<RectTransform>().sizeDelta.y);
            entry.GetComponent<LevelEntryItem>().SetupEntry(ID, info);
            levels.Add(ID, entry);

            ++ID;
        }

        if (buildMode) {
            GameObject entry = Instantiate(newLevelPrefab);

            entry.transform.SetParent(gameObject.transform);
            entry.transform.position = Vector2.zero;
            entry.transform.localScale = Vector3.one;

            yy = (GetComponent<RectTransform>().rect.y + entrySize/2f) + (ID * entrySize);
            sizeY += entrySize;
            entry.GetComponent<RectTransform>().localPosition = new Vector3(0f, -yy, 0);
            entry.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, entry.GetComponent<RectTransform>().sizeDelta.y);
            //entry.GetComponent<NewLevelEntryItem>().SetupEntry(ID, info);
            //levels.Add(ID, entry);

            //++ID;
        }

        // Block panel should not scroll past this Y value (starts at Y=0, scrolls towards this max)
        yMax = sizeY - parentRect.rect.height;

        Scrollbar bar = scrollBar.GetComponent<Scrollbar>();

        if (yMax < 0) {
            bar.size = 1f;
            scrollBar.GetComponent<Scrollbar>().value = 0f;
        } else {
            float barScale = 1500f / Mathf.Pow(yMax, 1 / 4f);

            float barMin = 10f;
            barScale = barScale < barMin ? barMin : barScale;

            bar.size = 1 / Mathf.Sqrt(yMax / barScale);
        }
    }

    public void SetSelectedLevelID(int id) {
        if (selectedLevel != -1) {
            levels[selectedLevel].GetComponent<LevelEntryItem>().setSelected(false);
        }
        selectedLevel = id;
        if (selectedLevel != -1) {
            levels[selectedLevel].GetComponent<LevelEntryItem>().setSelected(true);
            selectedName.GetComponent<Text>().text = levels[selectedLevel].GetComponent<LevelEntryItem>().levelName;
            selectedAuthor.GetComponent<Text>().text = levels[selectedLevel].GetComponent<LevelEntryItem>().levelAuthor;
            selectedDescription.GetComponent<Text>().text = levels[selectedLevel].GetComponent<LevelEntryItem>().levelDescription;

            playButton.GetComponent<Button>().interactable = true;
            buildButton.GetComponent<Button>().interactable = true;
        } else {
            selectedName.GetComponent<Text>().text = "";
            selectedAuthor.GetComponent<Text>().text = "";
            selectedDescription.GetComponent<Text>().text = "";

            playButton.GetComponent<Button>().interactable = false;
            buildButton.GetComponent<Button>().interactable = false;
        }
    }

    public void ClearSelectedItems() {
        selectedName.GetComponent<Text>().text = "";
        selectedAuthor.GetComponent<Text>().text = "";
        selectedDescription.GetComponent<Text>().text = "";
        if (selectedLevel != -1) {
            levels[selectedLevel].GetComponent<LevelEntryItem>().setSelected(false);
        }
        selectedLevel = -1;
        playButton.GetComponent<Button>().interactable = false;
        buildButton.GetComponent<Button>().interactable = false;
    }

    public LevelEntryItem GetSelectedLevelEntryItem() {
        if (selectedLevel != -1) {
            return levels[selectedLevel].GetComponent<LevelEntryItem>();
        }
        return null;
    }

    void Update() {
        transform.localPosition = new Vector3(transform.localPosition.x, parentRect.localPosition.y + scrollBar.GetComponent<Scrollbar>().value * yMax /*+ parentRect.rect.height / 2*/, transform.localPosition.z);
        if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == scrollBar) {
            EventSystem.current.SetSelectedGameObject(null);
        }
    }
}
