﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExitButton : MonoBehaviour {

    public float finalButtonX;
    public float timeUntilMove;
    public float speed;

    void Start() {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    public void MoveIn() {
        StartCoroutine(Move());
    }

    private IEnumerator Move() {
        yield return new WaitForSeconds(timeUntilMove);
        float startTime = Time.time;
        while (GetComponent<RectTransform>().anchoredPosition.x < finalButtonX - 0.1) {
            float timeElapsed = Time.time - startTime;
            GetComponent<RectTransform>().anchoredPosition = new Vector2(
                Mathf.Lerp(GetComponent<RectTransform>().anchoredPosition.x, finalButtonX, timeElapsed / speed / 2),
                GetComponent<RectTransform>().anchoredPosition.y);
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        GetComponent<RectTransform>().anchoredPosition = new Vector2(
                finalButtonX, GetComponent<RectTransform>().anchoredPosition.y);
    }

    void OnClick() {
        Application.Quit();
    }
}
