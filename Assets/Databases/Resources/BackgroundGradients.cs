﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BackgroundColors {
    public enum Stage { CUTMAN, NONE };

    public static Dictionary<Stage, BackgroundColor> backgrounds = new Dictionary<Stage, BackgroundColor>() {
        { Stage.CUTMAN, new BackgroundColor(new Color(0, 232/255f, 216/255f)) }
    };
}
